package l6_oopexample;

import java.util.Scanner;

public class Moto extends Transport{
    
    boolean m_bIsHaveSidecar = false;

    public Moto(){
        super();
    }
    
    public Moto(String strManufactureName, String strModelName, float fPrice, int nWheelCount) {
        super(strManufactureName, strModelName, fPrice, nWheelCount);
    }

    @Override
    public void inputData() {
        Scanner scan = new Scanner(System.in);
        
        System.out.println("Введите производителя мотоцикла:");
        String strManufactureName = scan.nextLine();
        
        System.out.println("Введите Марку мотоцикла:");
        String strModelName = scan.nextLine();
        
        System.out.println("Введите Стоимость мотоцикла:");
        float fPrice = scan.nextFloat();
        
        System.out.println("Введите кол-во колес мотоцикла:");
        int nWhellCount = scan.nextInt();
        
        System.out.println("Наличие коляски?\n(1 - Есть, 0 - Нет)");
        boolean bIsHaveSidecar = scan.nextByte()!=0;
        
        this.setManufactureName(strManufactureName);
        this.setModelName(strModelName);
        this.setPrice(fPrice);
        this.setWheelCount(nWhellCount);
        this.setHaveSidecar(bIsHaveSidecar);
    }

    @Override
    public void outputData() {
        String strOut = "Мотоцикл фирмы " + getManufactureName()
                + " модель " + getModelName()
                + " стоимостью " + getPrice()
                + " имеет " + getWheelCount() + " колес";
        if (isHaveSidecar()){
            strOut += " с коляской";
        } else {
            strOut += " без коляски";
        }
        System.out.println(strOut);
    }

    @Override
    public boolean isValid(String strSearch) {
        return getManufactureName().contains(strSearch) || getModelName().contains(strSearch);
    }
    
    public boolean isHaveSidecar(){
        return m_bIsHaveSidecar;
    }
    
    public void setHaveSidecar(boolean bIsHaveSidecar){
        m_bIsHaveSidecar = bIsHaveSidecar;
    }
    
}
