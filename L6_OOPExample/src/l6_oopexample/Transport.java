package l6_oopexample;

public abstract class Transport {
    
    private String m_strManufactureName = "";
    private String m_strModelName = "";
    private float m_fPrice = 0;
    private int m_nWheelCount = 0;
    
    public Transport(){
        setManufactureName("");
        setModelName("");
        setPrice(0);
        setWheelCount(0);
    }
    
    public Transport(String strManufactureName, String strModelName,
            float fPrice, int nWheelCount){
        setManufactureName(strManufactureName);
        setModelName(strModelName);
        setPrice(fPrice);
        setWheelCount(nWheelCount);
    }
    
    public String getManufactureName(){
        return m_strManufactureName;
    }
    
    public void setManufactureName(String strManufactureName){
        m_strManufactureName = strManufactureName;
    }
    
    public String getModelName(){
        return m_strModelName;
    }
    
    public void setModelName(String strModelName){
        m_strModelName = strModelName;
    }
    
    public float getPrice(){
        return m_fPrice;
    }
    
    public void setPrice(float fPrice){
        m_fPrice = fPrice;
    }
    
    public int getWheelCount(){
        return m_nWheelCount;
    }
    
    public void setWheelCount(int nWheelCount){
        m_nWheelCount = nWheelCount;
    }
    
    public abstract void inputData();
    public abstract void outputData();
    public abstract boolean isValid(String strSearch);
}
