package l6_oopexample;

import java.util.Scanner;

public class Car extends Transport{
    
    public static final int TYPE_SEDAN = 1;
    public static final int TYPE_HACHBACK = 2;
    public static final int TYPE_CUPE = 3;
    
    private int m_nType = TYPE_SEDAN;
    
    public Car(){
        super();
    }
    
    public Car(String strManufactureName, String strModelName, float fPrice, int nWheelCount) {
        super(strManufactureName, strModelName, fPrice, nWheelCount);
    }

    @Override
    public void inputData() {
        Scanner scan = new Scanner(System.in);
        
        System.out.println("Введите производителя авто:");
        String strManufactureName = scan.nextLine();
        
        System.out.println("Введите Марку авто:");
        String strModelName = scan.nextLine();
        
        System.out.println("Введите Стоимость авто:");
        float fPrice = scan.nextFloat();
        
        System.out.println("Введите кол-во колес авто:");
        int nWheelCount = scan.nextInt();
        
        System.out.println("Тип кузова:");
        System.out.println(TYPE_SEDAN + " - " + getNameTypeById(TYPE_SEDAN));
        System.out.println(TYPE_HACHBACK + " - " + getNameTypeById(TYPE_HACHBACK));
        System.out.println(TYPE_CUPE + " - " + getNameTypeById(TYPE_CUPE));
        int nType = scan.nextInt();
        
        
        this.setManufactureName(strManufactureName);
        this.setModelName(strModelName);
        this.setPrice(fPrice);
        this.setWheelCount(nWheelCount);
        this.setType(nType);
    }

    @Override
    public void outputData() {
        String strOut = "Автомобиль фирмы " + getManufactureName()
                + " модель " + getModelName()
                + " в кузове " + getNameTypeById(getType())
                + " стоимостью " + getPrice()
                + " имеет " + getWheelCount() + " колес";
        System.out.println(strOut);
    }

    @Override
    public boolean isValid(String strSearch) {
        return getManufactureName().contains(strSearch) 
                || getModelName().contains(strSearch) 
                || getNameTypeById(getType()).contains(strSearch);
    }
    
    public int getType(){
        return m_nType;
    }
    
    public void setType(int nType){
        m_nType = nType;
    }
    
    public static String getNameTypeById(int nType){
        String strResult = "";
        switch(nType){
            case TYPE_SEDAN:
                strResult = "Sedan";
                break;
            case TYPE_HACHBACK:
                strResult = "Hachback";
                break;
            case TYPE_CUPE:
                strResult = "Cupe";
        }
        return strResult;
    }
}
