
package l6_oopexample;

import java.util.ArrayList;
import java.util.Scanner;

public class L6_OOPExample {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        ArrayList<Transport> arrData = new ArrayList<>();
        
        do{
            System.out.println("");
            System.out.println("");
            System.out.println("Действие:");
            System.out.println("1. Добавить элемент.");
            System.out.println("2. Вывести весь список.");
            System.out.println("3. Поиск.");
            System.out.println("4. Удалить весь список.");
            System.out.println("0. Выход");
            int nType = scan.nextInt();
            switch(nType){
                case 1:
                    System.out.println("Тип:");
                    System.out.println("1. Мотоцикл");
                    System.out.println("2. Автомобиль");
                    nType = scan.nextInt();
                    Transport item;
                    if (nType==1){
                        item = new Moto();
                    } else {
                        item = new Car();
                    }
                    item.inputData();
                    arrData.add(item);
                    break;
                case 2:
                    for (Transport transport:arrData){
                       transport.outputData();
                       if (transport.getClass().equals(Car.class)){
                           System.out.println("^^Автомобиль^^");
                       }
                    }
                    break;
                case 3:
                    scan.nextLine(); //для избавления от пустой строки, после ввода числа
                    String strSearch = scan.nextLine();
                    for (Transport transport:arrData){
                       if (transport.isValid(strSearch)){
                           transport.outputData();
                       }
                    }
                    break;
                case 4:
                    arrData.clear();
                    break;
                case 0:
                    return;
                default:
                    System.out.println("Выбрано не корректное значение. Повторите ввод");
                    break;
            }
            
        } while (true);
    }
    
}
