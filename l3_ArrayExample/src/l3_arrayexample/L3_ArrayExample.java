package l3_arrayexample;

import java.util.Scanner;

public class L3_ArrayExample {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int nCount = 0;
        do{
            System.out.println("Get size:");
            nCount = scan.nextInt();
        } while(nCount<=0);
        
        float[] arrData = new float[nCount];
        
        for (int i=0; i<arrData.length; i++){
            do{
                System.out.println("Enter [" + (i+1)+"]:");
                arrData[i] = scan.nextFloat();
            } while(arrData[i]<=0);
        }
        
        float fSum = 0;
        
        for (float fVal:arrData){
            fSum+=fVal;
        }
        
        System.out.println("Sum: " + fSum);
        
    }
    
}
