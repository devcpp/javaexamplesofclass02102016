package l3_arraysortexample;

import java.util.Scanner;

public class L3_ArraySortExample {

    public static void main(String[] args) {
       Scanner scan = new Scanner(System.in);
        int nCount = 0;
        do{
            System.out.println("Get size:");
            nCount = scan.nextInt();
        } while(nCount<=0);
        
        int[] arrData = new int[nCount];
        
        //Enter Data
        for (int i=0; i<arrData.length;i++){
            arrData[i] = scan.nextInt();
        }
        
        //Sort Data
        boolean bIsCahged = false;
        int j = 1;
        do{
            bIsCahged = false;
            for (int i=0; i<arrData.length-j;i++){
                if (arrData[i]<arrData[i+1]){
                    bIsCahged = true;
                    int nTmp = arrData[i]; 
                    arrData[i] = arrData[i+1];
                    arrData[i+1] = nTmp;
                }
            }
            j++;
        } while(bIsCahged);
        
        for (int nVal:arrData){
            System.out.print(" " + nVal);
        }
    }
    
}
