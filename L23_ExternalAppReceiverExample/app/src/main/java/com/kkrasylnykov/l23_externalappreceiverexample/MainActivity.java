package com.kkrasylnykov.l23_externalappreceiverexample;

import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {
    private static final String AUTHORITY = "com.kkrasylnykov.l12_datasaveexamples.contentProveder.DBContentProvider.FFFC48566F9F1";

    public static final String TABLE_NAME_USER_INFO = "_USER_INFO";

    public static final Uri USER_INFO_URI    = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME_USER_INFO);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Cursor cursor = getContentResolver().query(USER_INFO_URI,null,null,null,null);
        if (cursor!=null && cursor.moveToFirst()){
            do{
                long nId = cursor.getLong(0);
                String strName = cursor.getString(1);
                String strSName = cursor.getString(2);
                String strPhone = cursor.getString(3);
                String strAdress = cursor.getString(4);

                Log.d("devcpp","nId -> " + nId);
                Log.d("devcpp","strName -> " + strName);
                Log.d("devcpp","strSName -> " + strSName);
                Log.d("devcpp","strPhone -> " + strPhone);
                Log.d("devcpp","strAdress -> " + strAdress);



                if (nId%2==0){
                    String strColumnName = cursor.getColumnName(0);
                    String strSelect = strColumnName + "=?";
                    String[] arrArg = new String[]{Long.toString(nId)};
                    getContentResolver().delete(USER_INFO_URI,strSelect,arrArg);
                }
            }while (cursor.moveToNext());

            cursor.close();
        }
    }
}
