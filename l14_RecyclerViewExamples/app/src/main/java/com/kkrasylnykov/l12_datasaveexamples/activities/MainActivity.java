package com.kkrasylnykov.l12_datasaveexamples.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.kkrasylnykov.l12_datasaveexamples.R;
import com.kkrasylnykov.l12_datasaveexamples.adapters.UserInfoRecyclerAdapter;
import com.kkrasylnykov.l12_datasaveexamples.model.TmpModel;
import com.kkrasylnykov.l12_datasaveexamples.toolsAndConstants.AppSettings;
import com.kkrasylnykov.l12_datasaveexamples.model.UserInfo;
import com.kkrasylnykov.l12_datasaveexamples.model.engines.UserInfoEngine;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, UserInfoRecyclerAdapter.OnClickUserInfoListener {

    private final static int REQ_CODE_TERMS = 1001;

    private RecyclerView m_recyclerView = null;
    private UserInfoRecyclerAdapter m_adapter = null;
    private ArrayList<Object> m_arrData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppSettings settings = new AppSettings(this);

        m_recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        m_recyclerView.setLayoutManager(layoutManager);
        m_adapter = new UserInfoRecyclerAdapter(m_arrData);
        m_adapter.setOnClickUserInfoListener(this);
        m_recyclerView.setAdapter(m_adapter);

        View addButton = findViewById(R.id.add);
        View removeAllButton = findViewById(R.id.removeAll);
        addButton.setOnClickListener(this);
        removeAllButton.setOnClickListener(this);

        View addItem3 = findViewById(R.id.addItem3);
        View removeItem3 = findViewById(R.id.removeItem3);
        View updateItem3 = findViewById(R.id.updateItem3);

        addItem3.setOnClickListener(this);
        removeItem3.setOnClickListener(this);
        updateItem3.setOnClickListener(this);

        if (!settings.isUserTermsAccepted()){
            Intent intent = new Intent(this, TermsActivity.class);
            startActivityForResult(intent, REQ_CODE_TERMS);
        }

        if (settings.isFirstStart()){
            settings.setIsFirstStart(false);

            UserInfoEngine engine = new UserInfoEngine(this);
            for (int i=0; i<500; i++){
                UserInfo userInfo = new UserInfo();
                userInfo.setName("test"+i);
                userInfo.setSName("test_"+i);
                userInfo.setPhone("095"+i);
                userInfo.setAddress("adr"+i);
                engine.insertUser(userInfo);
            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        showInfo();
    }

    private void showInfo(){
        UserInfoEngine engine = new UserInfoEngine(this);
        ArrayList<UserInfo> arrData = engine.getAll();
        m_arrData.clear();
        m_arrData.add("Test first item");
        m_arrData.addAll(arrData);
        m_arrData.add("Test last item");
        m_adapter.notifyDataSetChanged();
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode==REQ_CODE_TERMS){
            if (resultCode==RESULT_CANCELED){
                finish();
            } else if (resultCode==RESULT_OK){
                AppSettings settings = new AppSettings(this);
                settings.setIsUserTermsAccepted(true);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.add:
                startActivity(new Intent(this, EditActivity.class));
                break;
            case R.id.removeAll:
                removeAllItems();
                break;
            case R.id.addItem3:
                TmpModel tmpModel = new TmpModel("test_name", "61000");
                m_arrData.add(3,tmpModel);
                m_adapter.notifyItemInserted(3);
                break;
            case R.id.removeItem3:
                m_arrData.remove(3);
                m_adapter.notifyItemRemoved(3);
                break;
            case R.id.updateItem3:
                TmpModel tmpModel2 = new TmpModel("test_name", "61789");
                m_arrData.set(3,tmpModel2);
                m_adapter.notifyItemChanged(3);
                break;
        }
    }

    private void removeAllItems(){
        UserInfoEngine engine = new UserInfoEngine(this);
        engine.removeAll();
        showInfo();
    }

    @Override
    public void onClickUserInfo(View view, long nId) {
        Intent intent = new Intent(this, EditActivity.class);
        intent.putExtra(EditActivity.KEY_ID, nId);
        startActivity(intent);
    }
}
