package com.kkrasylnykov.l12_datasaveexamples.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.kkrasylnykov.l12_datasaveexamples.R;

public class TermsActivity extends AppCompatActivity
        implements View.OnClickListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);

        View viewYes = findViewById(R.id.yes);
        View viewNo = findViewById(R.id.no);

        viewYes.setOnClickListener(this);
        viewNo.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.yes:
                setResult(RESULT_OK);
                break;
            case R.id.no:
                setResult(RESULT_CANCELED);
                break;
        }
        finish();
    }
}
