package com.kkrasylnykov.l12_datasaveexamples.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kkrasylnykov.l12_datasaveexamples.R;
import com.kkrasylnykov.l12_datasaveexamples.model.TmpModel;
import com.kkrasylnykov.l12_datasaveexamples.model.UserInfo;

import java.util.ArrayList;

public class UserInfoRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_USER_INFO = 1001;
    private static final int TYPE_TITLE = 1002;
    private static final int TYPE_TMP = 1003;

    private ArrayList<Object> m_arrData = null;

    private OnClickUserInfoListener m_Listener = null;

    public UserInfoRecyclerAdapter(ArrayList<Object> arrData){
        m_arrData = arrData;
    }

    public void setOnClickUserInfoListener(OnClickUserInfoListener clickUserInfoListener){
        m_Listener = clickUserInfoListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        View view = null;
        switch (viewType){
            case TYPE_USER_INFO:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_info, parent, false);
                holder = new UserInfoHolder(view);
                break;
            case TYPE_TITLE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_string, parent, false);
                holder = new StringHolder(view);
                break;
            case TYPE_TMP:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tmp, parent, false);
                holder = new TmpHolder(view);
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)){
            case TYPE_USER_INFO:
                final UserInfo userInfo = (UserInfo) m_arrData.get(position);
                UserInfoHolder userInfoHolder = (UserInfoHolder)holder;
                userInfoHolder.m_NameTextView.setText(userInfo.getName());
                userInfoHolder.m_SNameTextView.setText(userInfo.getSName());
                userInfoHolder.m_PhoneTextView.setText(userInfo.getPhone());
                userInfoHolder.m_AddresTextView.setText(userInfo.getAddress());
                userInfoHolder.m_RootView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (m_Listener!=null){
                            m_Listener.onClickUserInfo(v,userInfo.getId());
                        }
                    }
                });
                break;
            case TYPE_TITLE:
                String strTitle = (String) m_arrData.get(position);
                StringHolder stringHolder = (StringHolder) holder;
                stringHolder.m_TitleTextView.setText(strTitle);
                break;
            case TYPE_TMP:
                TmpModel tmpModel = (TmpModel) m_arrData.get(position);
                TmpHolder tmpHolder = (TmpHolder) holder;
                tmpHolder.m_nameTmpTextView.setText(tmpModel.getName());
                tmpHolder.m_postTmpTextView.setText(tmpModel.getPostCode());
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (m_arrData.get(position) instanceof UserInfo){
            return TYPE_USER_INFO;
        } else if (m_arrData.get(position) instanceof String){
            return TYPE_TITLE;
        } else if (m_arrData.get(position) instanceof TmpModel){
            return TYPE_TMP;
        }
        return -1;
    }

    @Override
    public int getItemCount() {
        return m_arrData.size();
    }

    private class UserInfoHolder extends RecyclerView.ViewHolder{
        View m_RootView = null;
        TextView m_NameTextView = null;
        TextView m_SNameTextView = null;
        TextView m_PhoneTextView = null;
        TextView m_AddresTextView = null;

        public UserInfoHolder(View itemView) {
            super(itemView);
            m_RootView = itemView;
            m_NameTextView = (TextView) m_RootView.findViewById(R.id.nameItem);
            m_SNameTextView = (TextView) m_RootView.findViewById(R.id.snameItem);
            m_PhoneTextView = (TextView) m_RootView.findViewById(R.id.phoneItem);
            m_AddresTextView = (TextView) m_RootView.findViewById(R.id.addressItem);
        }
    }

    private class StringHolder extends RecyclerView.ViewHolder{
        View m_RootView = null;
        TextView m_TitleTextView = null;

        public StringHolder(View itemView) {
            super(itemView);
            m_RootView = itemView;
            m_TitleTextView = (TextView) m_RootView.findViewById(R.id.titleTextView);
        }
    }

    private class TmpHolder extends RecyclerView.ViewHolder{
        View m_RootView = null;
        TextView m_nameTmpTextView = null;
        TextView m_postTmpTextView = null;

        public TmpHolder(View itemView) {
            super(itemView);
            m_RootView = itemView;
            m_nameTmpTextView = (TextView) m_RootView.findViewById(R.id.nameTMPTextView);
            m_postTmpTextView = (TextView) m_RootView.findViewById(R.id.postTMPTextView);
        }
    }

    public interface OnClickUserInfoListener{
        public void onClickUserInfo(View view, long nId);
    }
}
