package l31_observerexample;

import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Service {
    
    private ArrayList<OnChangeInfo> m_arrChangeInfo;
    
    private static Service m_instance;
    
    private Thread m_Thread;
    
    private Service(){
        m_arrChangeInfo = new ArrayList<>();
        m_Thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {                    
                    Random random = new Random();
                    long nTime = random.nextInt(20 - 5) + 5;
                    nTime*= 1000;
                    try {
                        Thread.sleep(nTime);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    System.out.println("Service -> Change");
                    ArrayList<OnChangeInfo> arrTmp = new ArrayList<>();
                    arrTmp.addAll(m_arrChangeInfo);
                    for (OnChangeInfo item:arrTmp){
                        item.onChange();
                    }
                }
            }
        });
        m_Thread.start();
    }
    
    public void addListener(OnChangeInfo item){
        m_arrChangeInfo.add(item);
    }
    
    public static synchronized Service getInstance(){
        if (m_instance==null){
            m_instance = new Service();
        }
        return m_instance;
    }

    
}
