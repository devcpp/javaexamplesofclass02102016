package l31_observerexample;

public interface OnChangeInfo {
    void onChange();
}
