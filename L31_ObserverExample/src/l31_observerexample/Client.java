
package l31_observerexample;

public class Client implements OnChangeInfo{
    long m_nId;
    
    public Client() {
        
        m_nId = System.currentTimeMillis();
        Service.getInstance().addListener(this);
    }

    @Override
    public void onChange() {
        System.out.println("Client -> " + m_nId + " -> onChange");
    }
    
}
