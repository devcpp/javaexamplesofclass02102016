package l31_observerexample;

public class L31_ObserverExample {


    public static void main(String[] args) {
        
        Service.getInstance().addListener(new OnChangeInfo() {
            @Override
            public void onChange() {
                System.out.println("main -> onChange");
                new Client();
            }
        });
    }
    
}
