package com.kkrasylnykov.l10_fragmentexample2.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kkrasylnykov.l10_fragmentexample2.R;

public class ViewerFragment extends Fragment {

    private View m_RootView = null;
    private TextView m_TextView = null;
    private String m_strText = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        m_RootView = inflater.inflate(R.layout.fragment_viewer, null);
        m_TextView = (TextView) m_RootView.findViewById(R.id.textView);
        if (m_strText!=null){
            m_TextView.setText(m_strText);
            m_strText = null;
        }
        return m_RootView;
    }

    public void setText(String strText){
        if (isAdded()){
            m_TextView.setText(strText);
        } else {
            m_strText = strText;
        }
    }
}
