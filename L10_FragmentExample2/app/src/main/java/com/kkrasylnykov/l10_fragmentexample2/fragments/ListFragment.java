package com.kkrasylnykov.l10_fragmentexample2.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.kkrasylnykov.l10_fragmentexample2.R;
import com.kkrasylnykov.l10_fragmentexample2.activites.MainActivity;

public class ListFragment extends Fragment implements View.OnClickListener {

    private View m_RootView = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        m_RootView = inflater.inflate(R.layout.fragment_list, null);
        View button = m_RootView.findViewById(R.id.button1);
        View button2 = m_RootView.findViewById(R.id.button2);
        View button3 = m_RootView.findViewById(R.id.button3);
        button.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        return m_RootView;
    }

    @Override
    public void onClick(View v) {
        String strText = ((Button)v).getText().toString();
        ((MainActivity)getActivity()).setTextToViewer(strText);
    }
}
