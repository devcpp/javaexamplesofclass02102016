package com.kkrasylnykov.l10_fragmentexample2.activites;

import android.app.FragmentTransaction;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.kkrasylnykov.l10_fragmentexample2.R;
import com.kkrasylnykov.l10_fragmentexample2.fragments.ListFragment;
import com.kkrasylnykov.l10_fragmentexample2.fragments.ViewerFragment;

public class MainActivity extends AppCompatActivity {

    private ListFragment m_listFragment = null;
    private ViewerFragment m_viewerFragment = null;

    private int m_nOrientation = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        m_listFragment = new ListFragment();
        m_viewerFragment = new ViewerFragment();

        m_nOrientation = getResources().getConfiguration().orientation;
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        if (m_nOrientation== Configuration.ORIENTATION_PORTRAIT){
            transaction.add(R.id.firstFrameLayout,m_listFragment);
        } else if (m_nOrientation== Configuration.ORIENTATION_LANDSCAPE){
            transaction.add(R.id.firstFrameLayout,m_listFragment);
            transaction.add(R.id.secondFrameLayout,m_viewerFragment);
        }
        transaction.commit();
    }

    public void setTextToViewer(String strText){
        if (m_nOrientation== Configuration.ORIENTATION_PORTRAIT){
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.add(R.id.firstFrameLayout,m_viewerFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }
        m_viewerFragment.setText(strText);

    }
}
