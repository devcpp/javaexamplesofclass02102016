package com.kkrasylnykov.l28_retrofitexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class MainActivity extends AppCompatActivity {
    private final static String BASE_URL = "http://xutpuk.pp.ua";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Retrofit client = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        UserApi userApi = client.create(UserApi.class);

        Call<UserInfo[]> call = userApi.getUsers();
        call.enqueue(new Callback<UserInfo[]>() {
            @Override
            public void onResponse(Response<UserInfo[]> response) {
                if (response.isSuccess()){
                    UserInfo[] result = response.body();
                    for (UserInfo item:result){
                        Log.d("devcpp", "response -> " + item.getId());
                        Log.d("devcpp", "response -> " + item.getName());
                        Log.d("devcpp", "response -> " + item.getSname());
                    }
                } else {
                    //TODO if 404 or 500 SERVER_CODE
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });

        Call<UserInfo> callUser = userApi.getUser(110);
        callUser.enqueue(new Callback<UserInfo>() {
            @Override
            public void onResponse(Response<UserInfo> response) {
                if (response.isSuccess()){
                    Log.d("devcpp", "response2 -> " + response.body().getId());
                    Log.d("devcpp", "response2 -> " + response.body().getName());
                    Log.d("devcpp", "response2 -> " + response.body().getSname());
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });

    }
}
