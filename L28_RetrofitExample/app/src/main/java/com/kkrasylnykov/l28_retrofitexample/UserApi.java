package com.kkrasylnykov.l28_retrofitexample;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;

public interface UserApi {
    @GET("/api/users.json")
    Call<UserInfo[]> getUsers();

    @GET("/api/users/{nId}.json")
    Call<UserInfo> getUser(@Path("nId") int nId);
}
