package com.kkrasylnykov.l30_patternsexample.patterns;

public class DoubletonExample {

    private static DoubletonExample instance;

    private DoubletonExample(){

    }

    public static synchronized DoubletonExample getInstance(){
        if(instance==null){
            instance = new DoubletonExample();
            return new DoubletonExample();
        }
        return instance;
    }
}
