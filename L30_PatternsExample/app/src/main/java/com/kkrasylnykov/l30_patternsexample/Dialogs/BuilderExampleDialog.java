package com.kkrasylnykov.l30_patternsexample.Dialogs;


import android.content.Context;
import android.view.View;
import android.widget.Adapter;

import org.json.JSONException;
import org.json.JSONObject;

public class BuilderExampleDialog {
    Context context;
    String strTitle;
    String strMessage;
    Adapter adapter;
    String strNamePositiveButton;
    String strNameNegativeButton;
    String strNameNeutralButton;
    View.OnClickListener positiveListener;
    View.OnClickListener negativeListener;
    View.OnClickListener neutralListener;

    protected BuilderExampleDialog(){

    }

    protected BuilderExampleDialog(Context context, String strTitle, String strMessage, Adapter adapter, String strNamePositiveButton, String strNameNegativeButton, String strNameNeutralButton, View.OnClickListener positiveListener, View.OnClickListener negativeListener, View.OnClickListener neutralListener) {
        this.context = context;
        this.strTitle = strTitle;
        this.strMessage = strMessage;
        this.adapter = adapter;
        this.strNamePositiveButton = strNamePositiveButton;
        this.strNameNegativeButton = strNameNegativeButton;
        this.strNameNeutralButton = strNameNeutralButton;
        this.positiveListener = positiveListener;
        this.negativeListener = negativeListener;
        this.neutralListener = neutralListener;
    }

    public static class Builder{
        Context context;
        String strTitle;
        String strMessage;
        Adapter adapter;
        String strNamePositiveButton;
        String strNameNegativeButton;
        String strNameNeutralButton;
        View.OnClickListener positiveListener;
        View.OnClickListener negativeListener;
        View.OnClickListener neutralListener;

        public Builder(Context context){
            context = context;
        }

        public void setTitle(int nId){
            strTitle = context.getString(nId);
        }

        public void setTitle(String strTitle){
            strTitle = strTitle;
        }

        public void setTitle(JSONObject object) throws JSONException {
            strTitle = object.getString("title");
        }

        //TODO Add set and get functions....

        public BuilderExampleDialog build(){
            return new BuilderExampleDialog(context, strTitle, strMessage,adapter,strNamePositiveButton,strNameNegativeButton,strNameNeutralButton,positiveListener,negativeListener,neutralListener);
        }
    }
}
