package com.kkrasylnykov.l30_patternsexample.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.kkrasylnykov.l30_patternsexample.Dialogs.BuilderExampleDialog;
import com.kkrasylnykov.l30_patternsexample.R;
import com.kkrasylnykov.l30_patternsexample.patterns.SingletonExample;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BuilderExampleDialog.Builder builder = new BuilderExampleDialog.Builder(this);

        BuilderExampleDialog dialog = builder.build();

        SingletonExample singletonExample = SingletonExample.getInstance();


    }
}
