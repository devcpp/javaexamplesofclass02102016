package l2_functionexample;

public class L2_FunctionExample {
    
    public static int pow(int nVal, int nPow){
        int nRes = 0;
        if (nPow==0){
            nRes = 1;
        } else {
            nRes = nVal * pow(nVal,nPow-1);
        }
        return nRes;
    }
    
    public static int pow(int nVal){
        int nRes = nVal*nVal;
        return nRes;
    }
    
    public static float pow(float fVal){
        float fRes = fVal*fVal;
        return fRes;
    }
    
    public static void main(String[] args) {
       int n = 10;
       int nPowN = pow(n,2);
       
       int a = 4;
       int nPowA = pow(a,3);
       
       int b = 2;
       int nPowB = pow(b,10);
       
       System.out.println("nPowN -> " + nPowN);
       System.out.println("nPowA -> " + nPowA);
       System.out.println("nPowB -> " + nPowB);
    }
}
