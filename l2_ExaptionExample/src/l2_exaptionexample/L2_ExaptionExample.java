package l2_exaptionexample;

import java.util.InputMismatchException;
import java.util.Scanner;

public class L2_ExaptionExample {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);      
        int nResult = 0;
        
        try{
            int nA = scan.nextInt();
            int nB = scan.nextInt();
            
            if(nB==13){
                throw new RuntimeException(" Не люблю делить на 13");
            }
            nResult = (int)(nA/nB);
        } /*catch (ArithmeticException e){
            System.out.println("Нельзя делить на 0!");
            return;
        }*/ catch(InputMismatchException e){
            System.out.println("Не верный формат числа!");
            return;
        } /*catch(Exception e){
            System.out.println("Произошла неизвестная ошибка!" + e.getMessage());
            return;
        }*/
        
        System.out.println("nResult = " + nResult);
    }
    
}
