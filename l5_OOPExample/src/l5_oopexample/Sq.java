package l5_oopexample;

public final class Sq extends Figure{
    
    public static final float PI = 3.14f;
    
    private float m_fSide = 0;
    private float m_fDiagonal = 0;
    
    private static int s_nCount = 0;
    private static long s_nFirstCreateTime = -1;
    
    static {
       s_nFirstCreateTime = (long) (System.currentTimeMillis() * PI); 
    }
    
    public Sq(){
        s_nCount++;
    }
    
    /**
     * Принимает на вход длину стороны
     * @param fSide длинна стороны
     */
    public Sq(float fSide){
        s_nCount++;
        setSide(fSide);
    }
    
    public boolean setSide(float fSide){
        boolean bResult = false;
        if (fSide>0){
            bResult = true;
            this.m_fSide = fSide;
            this.m_fDiagonal = (float) (m_fSide*Math.pow(2, 0.5));
        }
        return bResult;
    }
    
    public boolean setDiagonal(float fDiagonal){
        boolean bResult = false;
        if (fDiagonal>0){
            bResult = true;
            this.m_fDiagonal = fDiagonal;
            this.m_fSide = (float) (m_fDiagonal/Math.pow(2, 0.5));
        }
        return bResult;
    }
    
    public float getSide(){
        return m_fSide;
    }
    
    public float getDiagonal(){
        return m_fDiagonal;
    }
    
    public static int getCount(){
        return s_nCount;
    }

    @Override
    public float getS() {
        return getSide()*getSide();
    }

    @Override
    public float getP() {
        return getSide()*4;
    }

    @Override
    public boolean equals(Object obj) {
        boolean bResult = false;
        if (obj!=null){
            try{
               bResult = ((Sq)obj).getSide()==this.getSide();
            } catch (ClassCastException e){                
            }
        }
        return bResult;
    }

    
}
