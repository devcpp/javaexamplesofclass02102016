package l5_oopexample;

import java.util.Scanner;

public class L5_OOPExample {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        float r = 3 * Sq.PI;
        
        Sq s = new Sq();
        Sq s2 = new Sq(3);
        Sq s3 = new Sq(5);
        
        System.out.println("Что вы выбираете?");
        System.out.println("1. Сторону");
        System.out.println("2. Диоганаль");
        
        int nType = scanner.nextInt();
        float fData = 0;
        
        if (nType==1){
            System.out.println("Введите сторону:");
        } else if (nType==2){
            System.out.println("Введите диагональ:");
        } else {
            System.out.println("Вы ввели не коректное значение!");
            return;
        }
        fData = scanner.nextFloat();
        
        boolean bIsCorrect = false;
        if (nType==1){
            bIsCorrect = s.setSide(fData);
        } else if (nType==2){
            bIsCorrect = s.setDiagonal(fData);
        } 
        
        if (!bIsCorrect){
            System.out.println("Вы ввели не коректное значение!");
            return;
        }
        
        System.out.println("Сторона = " + s.getSide());
        System.out.println("Диагональ = " + s.getDiagonal());
        System.out.println("P = " + s.getP());
        System.out.println("S = " + s.getS());
        
        System.out.println("Сторона = " + s2.getSide());
        System.out.println("Диагональ = " + s2.getDiagonal());
        System.out.println("P = " + s2.getP());
        System.out.println("S = " + s2.getS());
        
        String str = "3";
        System.out.println("s2.equals(str) = " + s2.equals(str));
        System.out.println("s2.equals(s) = " + s2.equals(s));
        System.out.println("s2.equals(s3) = " + s2.equals(s3));
    }
    
}
