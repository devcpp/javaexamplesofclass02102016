package l5_oopexample;
public abstract class Figure {
    
    public abstract float getS();
    
    public abstract float getP();
    
    public final String getNameFirstParent(){
        return "Figure";
    }
    
}
