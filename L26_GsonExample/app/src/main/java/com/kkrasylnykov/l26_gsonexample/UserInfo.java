package com.kkrasylnykov.l26_gsonexample;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class UserInfo {
    @SerializedName("id")
    private long m_nId;
    @SerializedName("name")
    private String m_strName;
    @SerializedName("second_name")
    private String m_strSName;
    @SerializedName("bday")
    private int m_nBDay;
    @SerializedName("phones")
    private ArrayList<String> m_arrPhones;

    public UserInfo(){

    }

    public long getId() {
        return m_nId;
    }

    public void setId(long m_nId) {
        this.m_nId = m_nId;
    }

    public String getName() {
        return m_strName;
    }

    public void setName(String m_strName) {
        this.m_strName = m_strName;
    }

    public String getSName() {
        return m_strSName;
    }

    public void setSName(String m_strSName) {
        this.m_strSName = m_strSName;
    }

    public int getBDay() {
        return m_nBDay;
    }

    public void setBDay(int m_nBDay) {
        this.m_nBDay = m_nBDay;
    }

    public ArrayList<String> getPhones() {
        return m_arrPhones;
    }

    public void setPhones(ArrayList<String> m_arrPhones) {
        this.m_arrPhones = m_arrPhones;
    }

    @Override
    public String toString() {
        String strData = "m_nId -> " + m_nId +
                "; m_strName ->" + m_strName +
                "; m_strSName -> " + m_strSName +
                "; m_nBDay -> " + m_nBDay +
                "; m_arrPhones -> [";
        for (String strPhone:m_arrPhones){
            strData += strPhone + ",";
        }
        strData += "];";
        return strData;
    }
}
