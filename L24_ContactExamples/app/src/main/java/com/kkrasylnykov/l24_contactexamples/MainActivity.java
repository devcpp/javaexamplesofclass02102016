package com.kkrasylnykov.l24_contactexamples;

import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView m_TextView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        m_TextView = (TextView) findViewById(R.id.TextView);

        EditText editText = (EditText)findViewById(R.id.searchEditText);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                m_TextView.setText(getInfo(charSequence.toString()));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private String getInfo(String strSearch){
        String strResult = "";
        if (strSearch.isEmpty()){
            return strResult;
        }
        String strSearchEmail = "%" +strSearch + "%";

        String[] PROJECTION = new String[] { ContactsContract.RawContacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.Contacts.PHOTO_THUMBNAIL_URI,
                ContactsContract.CommonDataKinds.Email.DATA,
                ContactsContract.CommonDataKinds.Email.CONTACT_ID,
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.Contacts.HAS_PHONE_NUMBER};
        String strSelect = ContactsContract.CommonDataKinds.Email.DATA + " LIKE ? OR "
                + ContactsContract.Contacts.DISPLAY_NAME + " LIKE ?";
        String[] arrArg = new String[]{strSearchEmail, strSearchEmail};

        Cursor cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, PROJECTION,
                strSelect, arrArg, null);
        if (cursor!=null && cursor.moveToFirst()){
            do{
                String strId = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.CONTACT_ID));
                strResult += cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)) +"\n";
                strResult += cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA)) +"\n";
                strResult += cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.CONTACT_ID)) +"\n";

                Cursor curPhone = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{strId}, null);
                if (curPhone!=null && curPhone.moveToFirst()){
                    do {
                        strResult += curPhone.getString(curPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    }while (curPhone.moveToNext());
                    curPhone.close();
                }
                strResult +="\n*****\n";
            } while (cursor.moveToNext());
            cursor.close();
        }
        return strResult;
    }
}
