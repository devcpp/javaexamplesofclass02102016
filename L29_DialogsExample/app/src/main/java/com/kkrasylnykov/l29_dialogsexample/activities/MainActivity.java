package com.kkrasylnykov.l29_dialogsexample.activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import com.kkrasylnykov.l29_dialogsexample.R;
import com.kkrasylnykov.l29_dialogsexample.dialogs.CustomDialog;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button btnCustom;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btnAlertDialog).setOnClickListener(this);
        findViewById(R.id.btnWaitingDialog).setOnClickListener(this);
        findViewById(R.id.btnTimeDialog).setOnClickListener(this);
        findViewById(R.id.btnDateDialog).setOnClickListener(this);
        btnCustom = (Button) findViewById(R.id.btnCustomDialog);
        btnCustom.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnAlertDialog:
                AlertDialog.Builder builderAlertDIalog = new AlertDialog.Builder(this);
                builderAlertDIalog.setMessage("This is AlertDialog!");
                builderAlertDIalog.setTitle("Title");
                builderAlertDIalog.setPositiveButton("Positive", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(MainActivity.this, "Positive button", Toast.LENGTH_LONG).show();
                    }
                });
                builderAlertDIalog.setNegativeButton("Negative", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(MainActivity.this, "Negative button", Toast.LENGTH_LONG).show();
                    }
                });
                builderAlertDIalog.setCancelable(false);
                AlertDialog alertDialog = builderAlertDIalog.create();
                alertDialog.show();
                break;
            case R.id.btnWaitingDialog:
                final ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.setMessage("Waiting dialog....");
                progressDialog.setCancelable(false);
                progressDialog.show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                            }
                        });
                    }
                }, 2000);

                break;
            case R.id.btnTimeDialog:
                TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int i, int i1) {
                        Log.d("devcpp","h -> " + i + "; m -> " + i1);
                    }
                };
                Calendar calendar = Calendar.getInstance();
                int curH = calendar.get(Calendar.HOUR);
                int curM = calendar.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(this,onTimeSetListener, curH, curM, true);
                timePickerDialog.show();
                break;
            case R.id.btnDateDialog:
                DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        Log.d("devcpp","y -> " + i + "; m -> " + i1 + "; d -> " + i2);
                    }
                };
                Calendar calendarDate = Calendar.getInstance();
                int curY = calendarDate.get(Calendar.YEAR);
                int curMONTH = calendarDate.get(Calendar.MONTH);
                int curD = calendarDate.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(this, onDateSetListener, curY, curMONTH, curD);
                datePickerDialog.show();
                break;
            case R.id.btnCustomDialog:
                CustomDialog customDialog = new CustomDialog(this, btnCustom.getText().toString());
                customDialog.setOnPositiveClickListener(new CustomDialog.OnClickPositive() {
                    @Override
                    public void onClick(String strText) {
                        btnCustom.setText(strText);
                    }
                });
                customDialog.show();
                break;
        }
    }
}
