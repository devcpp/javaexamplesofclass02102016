package com.kkrasylnykov.l29_dialogsexample.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;

import com.kkrasylnykov.l29_dialogsexample.R;

public class CustomDialog extends Dialog implements View.OnClickListener {
    EditText editText;
    OnClickPositive positiveListener;
    String strText;

    public CustomDialog(Context context, String strText) {
        super(context);
        this.strText = strText;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_custom);

        getWindow().setBackgroundDrawable(new ColorDrawable(0));

        findViewById(R.id.btnPositive).setOnClickListener(this);
        findViewById(R.id.btnNegative).setOnClickListener(this);
        editText = (EditText) findViewById(R.id.editTextDialog);
        if (editText!=null){
            editText.setText(this.strText);
        }

    }

    public void setOnPositiveClickListener(OnClickPositive listener){
        positiveListener = listener;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnPositive:
                if (positiveListener!=null){
                    positiveListener.onClick(editText.getText().toString());
                }
                break;
        }
        dismiss();
    }

    public interface OnClickPositive{
        void onClick(String strText);
    }
}
