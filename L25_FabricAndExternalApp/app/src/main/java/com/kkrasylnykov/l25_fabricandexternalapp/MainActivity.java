package com.kkrasylnykov.l25_fabricandexternalapp;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.*;
import io.fabric.sdk.android.BuildConfig;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQ_CODE_GALLARY = 101;

    private EditText m_editInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        ImageView imageView1 = (ImageView) findViewById(R.id.img1);
        ImageView imageView2 = (ImageView) findViewById(R.id.img2);
        ImageView imageView3 = (ImageView) findViewById(R.id.img3);

        Drawable img1 = ContextCompat.getDrawable(this, R.drawable.ic_img1);
        Drawable img2 = ContextCompat.getDrawable(this, R.drawable.ic_img2);
        Drawable img3 = ContextCompat.getDrawable(this, R.drawable.ic_img1);
        img1.setColorFilter(ContextCompat.getColor(this, android.R.color.holo_red_dark), PorterDuff.Mode.SRC_ATOP);
        imageView1.setImageDrawable(img1);

        img2.setColorFilter(ContextCompat.getColor(this, android.R.color.holo_blue_bright), PorterDuff.Mode.SRC_ATOP);
        imageView2.setImageDrawable(img2);

        img3.setColorFilter(ContextCompat.getColor(this, android.R.color.holo_green_dark), PorterDuff.Mode.SRC_ATOP);
        imageView3.setImageDrawable(img3);

        m_editInfo = (EditText) findViewById(R.id.editInfo);
        m_editInfo.setText(Settings.VERSION_TEST);


        findViewById(R.id.btnWeb).setOnClickListener(this);
        findViewById(R.id.btnPhone1).setOnClickListener(this);
        findViewById(R.id.btnPhone2).setOnClickListener(this);
        findViewById(R.id.btnEmail).setOnClickListener(this);
        findViewById(R.id.btnPhotoGal).setOnClickListener(this);
        findViewById(R.id.btnFabric1).setOnClickListener(this);
        findViewById(R.id.btnFabric2).setOnClickListener(this);
        findViewById(R.id.btnFabric3).setOnClickListener(this);
        findViewById(R.id.btnFabric4).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        String strData = m_editInfo.getText().toString();

        switch (view.getId()){
            case R.id.btnWeb:
                if (strData.indexOf("http://")<0){
                    strData = "http://" + strData;
                }
                Intent intentWeb = new Intent();
                intentWeb.setAction(Intent.ACTION_VIEW);
                intentWeb.setData(Uri.parse(strData));
                startActivity(intentWeb);
                break;
            case R.id.btnPhone1:
                if (strData.indexOf("tel:")<0){
                    strData = "tel:" + strData;
                }
                Intent intentCall = new Intent();
                intentCall.setAction(Intent.ACTION_CALL);
                intentCall.setData(Uri.parse(strData));
                startActivity(intentCall);
                break;
            case R.id.btnPhone2:
                if (strData.indexOf("tel:")<0){
                    strData = "tel:" + strData;
                }
                Intent intentDial = new Intent();
                intentDial.setAction(Intent.ACTION_DIAL);
                intentDial.setData(Uri.parse(strData));
                startActivity(intentDial);
                break;
            case R.id.btnEmail:
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_EMAIL,  new String[] {"test_gmail@gmail.com"});
                intent.putExtra(Intent.EXTRA_SUBJECT, strData);
                intent.putExtra(Intent.EXTRA_TEXT, "I'm email body.");

                startActivity(Intent.createChooser(intent, "Choose App For Send Email"));
                break;

            case R.id.btnPhotoGal:
                Intent photo = new Intent();
                photo.setAction(Intent.ACTION_PICK);
                photo.setData(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(photo, REQ_CODE_GALLARY);
                break;
            case R.id.btnFabric1:
                int n = 0;
                int d = 1/n;
                break;
            case R.id.btnFabric2:
                throw new RuntimeException("Your click btnFabric2!!!!");
            case R.id.btnFabric3:
                throw new IndexOutOfBoundsException();
            case R.id.btnFabric4:
                throw new RuntimeException("Your click btnFabric4!!!!");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


    }
}
