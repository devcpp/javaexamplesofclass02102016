package com.kkrasylnykov.navigation;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.old_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open_drawer, R.string.close_drawer);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_nenu, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.item_settings);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.item_settings){
            Toast.makeText(this, getString(R.string.item_settings), Toast.LENGTH_LONG).show();
            setTitle(R.string.item_settings);
        } else  if (item.getItemId() == R.id.item_about){
            Toast.makeText(this, R.string.item_about, Toast.LENGTH_LONG).show();
            setTitle(R.string.item_about);
        }else if (item.getItemId() == R.id.item_faq){
            Toast.makeText(this, R.string.item_faq, Toast.LENGTH_LONG).show();
            setTitle(R.string.item_faq);
        }

        return true;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.item_settings){
            Toast.makeText(this, getString(R.string.item_settings), Toast.LENGTH_LONG).show();
            setTitle(R.string.item_settings);

        } else  if (item.getItemId() == R.id.item_about){
            Toast.makeText(this, R.string.item_about, Toast.LENGTH_LONG).show();
            setTitle(R.string.item_about);
        }else if (item.getItemId() == R.id.item_faq){
            Toast.makeText(this, R.string.item_faq, Toast.LENGTH_LONG).show();
            setTitle(R.string.item_faq);
        }
        return true;
    }
}
