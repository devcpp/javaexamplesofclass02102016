package com.kkrasylnykov.l8_activitesexample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int REQUEST_CODE_SECOND_ACTIVITY = 1234;

    private EditText m_infoEditText = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        View nextActivityButton = findViewById(R.id.nextActivityButtonMainActivity);
        nextActivityButton.setOnClickListener(this);

        m_infoEditText = (EditText) findViewById(R.id.nameEditTextMainctivity);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.nextActivityButtonMainActivity:
                toNextActivity();
                break;
        }
    }
    private void toNextActivity(){
        String strName = m_infoEditText.getText().toString();
        if (strName.isEmpty()){
            Toast.makeText(MainActivity.this, "Name is empty!!!!", Toast.LENGTH_LONG).show();
        } else {
            Intent intent = new Intent(MainActivity.this, SecondActivity.class);
            intent.putExtra(SecondActivity.KEY_NAME, strName);
            startActivityForResult(intent, REQUEST_CODE_SECOND_ACTIVITY);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("devcppL8","requestCode -> " + requestCode + "; resultCode -> " + resultCode + "; data -> " + data);
        if (requestCode==REQUEST_CODE_SECOND_ACTIVITY){
            if (resultCode==RESULT_OK && data!=null && data.getExtras()!=null){
                Bundle bundle = data.getExtras();
                Log.d("devcppL8","bundle -> KEY_RETURN_TEXT -> " + bundle.getString(SecondActivity.KEY_RETURN_TEXT));
            }
        }
    }
}
