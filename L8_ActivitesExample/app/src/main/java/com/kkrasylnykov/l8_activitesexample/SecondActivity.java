package com.kkrasylnykov.l8_activitesexample;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String KEY_NAME = "KEY_NAME";

    public static final String KEY_RETURN_TEXT = "KEY_RETURN_TEXT";

    EditText m_EditText = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        String strName = "";

        Intent intent = getIntent();
        if (intent!=null){
//            if (intent.hasExtra(KEY_NAME)){
//                strName = intent.getStringExtra(KEY_NAME);
//            }
            Bundle bundle = intent.getExtras();
            if (bundle!=null){
                strName = bundle.getString(KEY_NAME, "");
            }
        }

        View positiveButton = findViewById(R.id.positiveButtonSecondActivity);
        positiveButton.setOnClickListener(this);

        View negativeButton = findViewById(R.id.negativeButtonSecondActivity);
        negativeButton.setOnClickListener(this);

        m_EditText = (EditText) findViewById(R.id.infoEditTextSecondActivity);

        TextView infoTextView = (TextView) findViewById(R.id.infoTextViewSecondActivity);
        if (strName.isEmpty()){
            infoTextView.setText("This is Second Activity!!!!");
        } else {
            infoTextView.setText("Hello, " + strName + "!!!");
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.positiveButtonSecondActivity:
                String strText = m_EditText.getText().toString();
                Intent intent = new Intent();
                intent.putExtra(KEY_RETURN_TEXT,strText);
                setResult(RESULT_OK, intent);
                break;
            case R.id.negativeButtonSecondActivity:
                setResult(RESULT_CANCELED);
                break;
        }
        finish();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

    @Override
    public void finish() {
        super.finish();
    }
}
