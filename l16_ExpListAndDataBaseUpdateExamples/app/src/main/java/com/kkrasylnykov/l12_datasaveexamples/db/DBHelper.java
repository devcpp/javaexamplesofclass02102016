package com.kkrasylnykov.l12_datasaveexamples.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.kkrasylnykov.l12_datasaveexamples.toolsAndConstants.DBConstants;

public class DBHelper extends SQLiteOpenHelper {


    public DBHelper(Context context) {
        super(context, DBConstants.DB_NAME, null, DBConstants.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DBConstants.TABLE_NAME_USER_INFO +
                " (" + DBConstants.FIELD_USER_INFO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DBConstants.FIELD_USER_INFO_NAME + " TEXT NOT NULL, " +
                DBConstants.FIELD_USER_INFO_SNAME + " TEXT NOT NULL, " +
                DBConstants.FIELD_USER_INFO_ADRESS + " TEXT NOT NULL);");

        db.execSQL("CREATE TABLE " + DBConstants.TABLE_NAME_PHONES +
                " (" + DBConstants.FIELD_PHONES_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DBConstants.FIELD_PHONES_USER_ID + " INTEGER NOT NULL, " +
                DBConstants.FIELD_PHONES_PHONE + " TEXT NOT NULL);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion==1){
            db.execSQL("CREATE TABLE " + DBConstants.TABLE_NAME_USER_INFO + "_OLD" +
                    " (" + DBConstants.FIELD_USER_INFO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    DBConstants.FIELD_USER_INFO_NAME + " TEXT NOT NULL, " +
                    DBConstants.FIELD_USER_INFO_SNAME + " TEXT NOT NULL, " +
                    DBConstants.FIELD_OLD_1_USER_INFO_PHONE + " TEXT NOT NULL, " +
                    DBConstants.FIELD_USER_INFO_ADRESS + " TEXT NOT NULL);");
            Cursor cursor = db.query(DBConstants.TABLE_NAME_USER_INFO,null,null,null,null,null,null);
            if (cursor!=null){
                if (cursor.moveToFirst()){
                    do {
                        ContentValues values = new ContentValues();
                        values.put(DBConstants.FIELD_USER_INFO_NAME,
                                cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_USER_INFO_NAME)));
                        values.put(DBConstants.FIELD_USER_INFO_SNAME,
                                cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_USER_INFO_SNAME)));
                        values.put(DBConstants.FIELD_OLD_1_USER_INFO_PHONE,
                                cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_OLD_1_USER_INFO_PHONE)));
                        values.put(DBConstants.FIELD_USER_INFO_ADRESS,
                                cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_USER_INFO_ADRESS)));
                        db.insert(DBConstants.TABLE_NAME_USER_INFO + "_OLD",null,values);
                    } while (cursor.moveToNext());
                }
                cursor.close();
            }

            db.execSQL("DROP TABLE " + DBConstants.TABLE_NAME_USER_INFO);
            onCreate(db);

            cursor = db.query(DBConstants.TABLE_NAME_USER_INFO + "_OLD",null,null,null,null,null,null);
            if (cursor!=null){
                if (cursor.moveToFirst()){
                    do {
                        ContentValues valuesUserInfo = new ContentValues();
                        valuesUserInfo.put(DBConstants.FIELD_USER_INFO_NAME,
                                cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_USER_INFO_NAME)));
                        valuesUserInfo.put(DBConstants.FIELD_USER_INFO_SNAME,
                                cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_USER_INFO_SNAME)));
                        valuesUserInfo.put(DBConstants.FIELD_USER_INFO_ADRESS,
                                cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_USER_INFO_ADRESS)));
                        long nUserId = db.insert(DBConstants.TABLE_NAME_USER_INFO, null, valuesUserInfo);

                        ContentValues valuesPhones = new ContentValues();

                        valuesPhones.put(DBConstants.FIELD_PHONES_USER_ID, nUserId);
                        valuesPhones.put(DBConstants.FIELD_PHONES_PHONE,
                                cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_OLD_1_USER_INFO_PHONE)));

                        db.insert(DBConstants.TABLE_NAME_PHONES, null, valuesPhones);
                    } while (cursor.moveToNext());
                }
                cursor.close();

                db.execSQL("DROP TABLE " + DBConstants.TABLE_NAME_USER_INFO + "_OLD");
            }
        }

    }
}
