package l4_setexample;

import java.util.HashSet;
import java.util.Scanner;

public class L4_SetExample {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        HashSet<Integer> setData = new HashSet<>();
        
        setData.add(new Integer(10));
        setData.add(new Integer(2));
        setData.add(new Integer(5));
        setData.add(new Integer(2));
        setData.add(new Integer(6));
        
        
        System.out.println(" setData ->" + setData.size());
        
        for(Integer nVal:setData){
            System.out.print(" nVal ->" + nVal);
        }
        System.out.println();
        System.out.println("***********");
        
        System.out.println(" setData.remove(6) ->" + setData.remove(6));
        System.out.println(" setData.remove(11) ->" + setData.remove(11));
        for(Integer nVal:setData){
            System.out.print(" nVal ->" + nVal);
        }
        /*for(int i=0;i<11;i++){
            if (setData.contains(i)){
                System.out.print(" nVal ->" + i);
            }
        }*/
        System.out.println();
    }
    
}
