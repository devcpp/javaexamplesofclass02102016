package com.kkrasylnykov.l12_datasaveexamples.adapters;

import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kkrasylnykov.l12_datasaveexamples.R;
import com.kkrasylnykov.l12_datasaveexamples.model.UserInfo;

import java.util.ArrayList;


public class UserInfoAdapter extends BaseAdapter {

    private ArrayList<UserInfo> m_arrData = null;

    public UserInfoAdapter(ArrayList<UserInfo> arrData){
        m_arrData = arrData;
    }

    @Override
    public int getCount() {
        return m_arrData.size();
    }

    @Override
    public Object getItem(int position) {
        return m_arrData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return ((UserInfo)getItem(position)).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView textView = null;
        if (convertView==null){
            Log.d("devcpp","create new view -> " + position);
            textView = new TextView(parent.getContext());
            AbsListView.LayoutParams layoutParams = new AbsListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            textView.setLayoutParams(layoutParams);
        } else {
            Log.d("devcpp","re-use view -> " + position);
            textView = (TextView) convertView;
        }

        if (position%5==0){
            textView.setBackgroundColor(Color.argb(255,255,0,0));
        } else {
            textView.setBackgroundColor(Color.argb(255,255,255,255));
        }
        UserInfo userInfo = (UserInfo)getItem(position);
        textView.setText(userInfo.getName() + " "
                + userInfo.getSName() + "\n"
                + userInfo.getPhone() + "\n" +  userInfo.getAddress()+"\n");
        return textView;
    }
}
