package com.kkrasylnykov.l12_datasaveexamples.model.engines;

import android.content.Context;

import com.kkrasylnykov.l12_datasaveexamples.model.UserInfo;
import com.kkrasylnykov.l12_datasaveexamples.model.wrappers.dbWrappers.UserInfoDBWrapper;

import java.util.ArrayList;

public class UserInfoEngine {

    private Context m_Context = null;

    public UserInfoEngine(Context context){
        m_Context  = context;
    }


    public ArrayList<UserInfo> getAll(){
        UserInfoDBWrapper dbWrapper = new UserInfoDBWrapper(m_Context);
        return dbWrapper.getAll();
    }

    public UserInfo getUserById(long nId){
        UserInfoDBWrapper dbWrapper = new UserInfoDBWrapper(m_Context);
        return dbWrapper.getUserById(nId);
    }

    public boolean updateUser(UserInfo item){
        UserInfoDBWrapper dbWrapper = new UserInfoDBWrapper(m_Context);
        return dbWrapper.updateUser(item);
    }

    public boolean insertUser(UserInfo item){
        UserInfoDBWrapper dbWrapper = new UserInfoDBWrapper(m_Context);
        return dbWrapper.insertUser(item);
    }

    public boolean removeUser(UserInfo item){
        UserInfoDBWrapper dbWrapper = new UserInfoDBWrapper(m_Context);
        return dbWrapper.removeUser(item);
    }

    public boolean removeAll(){
        UserInfoDBWrapper dbWrapper = new UserInfoDBWrapper(m_Context);
        return dbWrapper.removeAll();
    }
}
