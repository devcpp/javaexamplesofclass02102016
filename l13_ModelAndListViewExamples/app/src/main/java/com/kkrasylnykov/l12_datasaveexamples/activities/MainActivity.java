package com.kkrasylnykov.l12_datasaveexamples.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.kkrasylnykov.l12_datasaveexamples.R;
import com.kkrasylnykov.l12_datasaveexamples.adapters.UserInfoAdapter;
import com.kkrasylnykov.l12_datasaveexamples.toolsAndConstants.AppSettings;
import com.kkrasylnykov.l12_datasaveexamples.model.UserInfo;
import com.kkrasylnykov.l12_datasaveexamples.model.engines.UserInfoEngine;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    private final static int REQ_CODE_TERMS = 1001;

    private ListView m_listView = null;
    private UserInfoAdapter m_adapter = null;
    private ArrayList<UserInfo> m_arrData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppSettings settings = new AppSettings(this);

        m_listView = (ListView) findViewById(R.id.listView);
        m_adapter = new UserInfoAdapter(m_arrData);
        m_listView.setAdapter(m_adapter);
        m_listView.setOnItemClickListener(this);


        View addButton = findViewById(R.id.add);
        View removeAllButton = findViewById(R.id.removeAll);
        addButton.setOnClickListener(this);
        removeAllButton.setOnClickListener(this);

        if (!settings.isUserTermsAccepted()){
            Intent intent = new Intent(this, TermsActivity.class);
            startActivityForResult(intent, REQ_CODE_TERMS);
        }

        if (settings.isFirstStart()){
            settings.setIsFirstStart(false);

            UserInfoEngine engine = new UserInfoEngine(this);
            for (int i=0; i<500; i++){
                UserInfo userInfo = new UserInfo();
                userInfo.setName("test"+i);
                userInfo.setSName("test_"+i);
                userInfo.setPhone("095"+i);
                userInfo.setAddress("adr"+i);
                engine.insertUser(userInfo);
            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        showInfo();
    }

    private void showInfo(){
        UserInfoEngine engine = new UserInfoEngine(this);
        ArrayList<UserInfo> arrData = engine.getAll();
        m_arrData.clear();
        m_arrData.addAll(arrData);
        m_adapter.notifyDataSetChanged();
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode==REQ_CODE_TERMS){
            if (resultCode==RESULT_CANCELED){
                finish();
            } else if (resultCode==RESULT_OK){
                AppSettings settings = new AppSettings(this);
                settings.setIsUserTermsAccepted(true);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.add:
                startActivity(new Intent(this, EditActivity.class));
                break;
            case R.id.removeAll:
                removeAllItems();
                break;
        }
    }

    private void removeAllItems(){
        UserInfoEngine engine = new UserInfoEngine(this);
        engine.removeAll();
        showInfo();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(this, EditActivity.class);
        intent.putExtra(EditActivity.KEY_ID, id);
        startActivity(intent);
    }
}
