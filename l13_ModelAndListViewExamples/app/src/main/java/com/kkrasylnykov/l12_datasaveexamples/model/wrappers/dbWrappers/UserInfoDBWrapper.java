package com.kkrasylnykov.l12_datasaveexamples.model.wrappers.dbWrappers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kkrasylnykov.l12_datasaveexamples.toolsAndConstants.DBConstants;
import com.kkrasylnykov.l12_datasaveexamples.db.DBHelper;
import com.kkrasylnykov.l12_datasaveexamples.model.UserInfo;

import java.util.ArrayList;

public class UserInfoDBWrapper {

    private DBHelper m_dbHelper = null;
    private String m_strTableName = "";

    public UserInfoDBWrapper(Context context){
        m_dbHelper = new DBHelper(context);
        m_strTableName = DBConstants.TABLE_NAME_USER_INFO;
    }

    public ArrayList<UserInfo> getAll(){
        ArrayList<UserInfo> arrResult = new ArrayList<>();
        SQLiteDatabase db = m_dbHelper.getReadableDatabase();
        Cursor cursor = db.query(m_strTableName,null,null,null,null,null,null);
        if (cursor!=null){
            if(cursor.moveToFirst()){
                do {
                    arrResult.add(new UserInfo(cursor));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        db.close();
        return arrResult;
    }

    public UserInfo getUserById(long nId){
        UserInfo result = null;
        SQLiteDatabase db = m_dbHelper.getReadableDatabase();
        String strSelect = DBConstants.FIELD_USER_INFO_ID + "=?";
        String[] arrArg = new String[]{Long.toString(nId)};
        Cursor cursor = db.query(m_strTableName,null,strSelect,arrArg,null,null,null);
        if (cursor!=null){
            if(cursor.moveToFirst()){
                result = new UserInfo(cursor);
            }
            cursor.close();
        }
        db.close();
        return result;
    }

    public boolean updateUser(UserInfo item){
        SQLiteDatabase db = m_dbHelper.getWritableDatabase();
        String strSelect = DBConstants.FIELD_USER_INFO_ID + "=?";
        String[] arrArg = new String[]{Long.toString(item.getId())};
        int nCountUdate = db.update(m_strTableName,item.getContentValues(),strSelect, arrArg);
        db.close();
        return nCountUdate==1;
    }

    public boolean insertUser(UserInfo item){
        SQLiteDatabase db = m_dbHelper.getWritableDatabase();
        long nId = db.insert(m_strTableName,null,item.getContentValues());
        db.close();
        return nId>=0;
    }

    public boolean removeUser(UserInfo item){
        SQLiteDatabase db = m_dbHelper.getWritableDatabase();
        String strSelect = DBConstants.FIELD_USER_INFO_ID + "=?";
        String[] arrArg = new String[]{Long.toString(item.getId())};
        int nCountUdate = db.delete(m_strTableName,strSelect, arrArg);
        db.close();
        return nCountUdate==1;
    }

    public boolean removeAll(){
        SQLiteDatabase db = m_dbHelper.getWritableDatabase();
        int nCountUdate = db.delete(m_strTableName,null, null);
        db.close();
        return nCountUdate>0;
    }
}
