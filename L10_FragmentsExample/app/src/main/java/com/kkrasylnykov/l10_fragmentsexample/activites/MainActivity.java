package com.kkrasylnykov.l10_fragmentsexample.activites;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.kkrasylnykov.l10_fragmentsexample.R;
import com.kkrasylnykov.l10_fragmentsexample.fragments.FirstFragment;
import com.kkrasylnykov.l10_fragmentsexample.fragments.SecondFragment;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private FirstFragment m_FirstFragment = null;
    private SecondFragment m_SecondFragment = null;
    private TextView m_TextView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        m_FirstFragment = new FirstFragment();
        m_SecondFragment = new SecondFragment();

        View addFirstFragment = findViewById(R.id.addFFragment);
        addFirstFragment.setOnClickListener(this);
        View removeFirstFragment = findViewById(R.id.removeFFragment);
        removeFirstFragment.setOnClickListener(this);

        View addSecondFragmentToSecond = findViewById(R.id.addSFragmentToSecond);
        addSecondFragmentToSecond.setOnClickListener(this);
        View removeSecondFragment = findViewById(R.id.removeSFragment);
        removeSecondFragment.setOnClickListener(this);

        m_TextView = (TextView) findViewById(R.id.textViewMainActivity);

    }

    @Override
    public void onClick(View v) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        switch (v.getId()){
            case R.id.addFFragment:
                transaction.replace(R.id.firstFrameLayout, m_FirstFragment);
                break;
            case R.id.removeFFragment:
                transaction.remove(m_FirstFragment);
                break;
            case R.id.addSFragmentToSecond:
                transaction.replace(R.id.secondFrameLayout, m_SecondFragment);
                break;
            case R.id.removeSFragment:
                transaction.remove(m_SecondFragment);
                break;
        }
        transaction.commit();
    }

    public void setTextToMainActivity(String strText){
        if (m_SecondFragment.isAdded()){
            m_SecondFragment.setTextToFragment(strText);
        } else {
            m_TextView.setText(strText);
        }

    }
}
