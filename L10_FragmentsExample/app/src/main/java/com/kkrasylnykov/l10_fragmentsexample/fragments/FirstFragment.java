package com.kkrasylnykov.l10_fragmentsexample.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.kkrasylnykov.l10_fragmentsexample.R;
import com.kkrasylnykov.l10_fragmentsexample.activites.MainActivity;

public class FirstFragment extends Fragment implements View.OnClickListener {

    private View m_RootView = null;
    private EditText m_EditText = null;
    private TextView m_TextView = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("devcpp", "Fragment -> onCreateView");
        m_RootView = inflater.inflate(R.layout.fragment_first,null);
        m_EditText = (EditText) m_RootView.findViewById(R.id.editTextFFragment);
        m_TextView = (TextView) m_RootView.findViewById(R.id.textViewFFragment);
        View button = m_RootView.findViewById(R.id.buttonFFragment);
        button.setOnClickListener(this);
        return m_RootView;
    }

    @Override
    public void onAttach(Context context) {
        Log.d("devcpp", "Fragment -> onAttach");
        super.onAttach(context);
    }

    @Override
    public void onStart() {
        Log.d("devcpp", "Fragment -> onStart");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.d("devcpp", "Fragment -> onResume");
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        String strText = m_EditText.getText().toString();
        ((MainActivity)getActivity()).setTextToMainActivity(strText);
    }
}
