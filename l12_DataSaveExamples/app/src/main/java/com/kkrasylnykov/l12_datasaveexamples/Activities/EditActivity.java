package com.kkrasylnykov.l12_datasaveexamples.Activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kkrasylnykov.l12_datasaveexamples.R;
import com.kkrasylnykov.l12_datasaveexamples.ToolsAndConstants.DBConstants;
import com.kkrasylnykov.l12_datasaveexamples.db.DBHelper;

public class EditActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String KEY_ID = "KEY_ID";

    private EditText m_nameEditText = null;
    private EditText m_snameEditText = null;
    private EditText m_phoneEditText = null;
    private EditText m_adressEditText = null;

    private long m_nId = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        m_nameEditText = (EditText) findViewById(R.id.name);
        m_snameEditText = (EditText) findViewById(R.id.sname);
        m_phoneEditText = (EditText) findViewById(R.id.phone);
        m_adressEditText = (EditText) findViewById(R.id.adress);

        Intent intent = getIntent();
        if (intent!=null){
            Bundle bundle = intent.getExtras();
            if (bundle!=null){
                m_nId = bundle.getLong(KEY_ID, -1);
            }
        }

        Button addView = (Button) findViewById(R.id.addItem);
        addView.setOnClickListener(this);

        if (m_nId>-1){
            DBHelper dbHelper = new DBHelper(this);
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            String strSelect = DBConstants.FIELD_USER_INFO_ID + "=?";
            String[] arrSelectArgs = new String[]{Long.toString(m_nId)};
            Cursor cursor = db.query(DBConstants.TABLE_NAME_USER_INFO, null, strSelect, arrSelectArgs, null, null, null);
            if (cursor!=null){
                if (cursor.moveToFirst()){
                    String strName = cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_USER_INFO_NAME));
                    String strSName = cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_USER_INFO_SNAME));
                    String strPhone = cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_USER_INFO_PHONE));
                    String strAdress = cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_USER_INFO_ADRESS));

                    m_nameEditText.setText(strName);
                    m_snameEditText.setText(strSName);
                    m_phoneEditText.setText(strPhone);
                    m_adressEditText.setText(strAdress);
                }
                cursor.close();
            }
            db.close();
            addView.setText("Update");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.addItem:
                addItem();
                break;
        }
    }

    private void addItem(){
        DBHelper dbHelper = new DBHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DBConstants.FIELD_USER_INFO_NAME, m_nameEditText.getText().toString());
        values.put(DBConstants.FIELD_USER_INFO_SNAME, m_snameEditText.getText().toString());
        values.put(DBConstants.FIELD_USER_INFO_PHONE, m_phoneEditText.getText().toString());
        values.put(DBConstants.FIELD_USER_INFO_ADRESS, m_adressEditText.getText().toString());

        if (m_nId>-1){
            String strSelect = DBConstants.FIELD_USER_INFO_ID + "=?";
            String[] arrSelectArgs = new String[]{Long.toString(m_nId)};
            db.update(DBConstants.TABLE_NAME_USER_INFO,values,strSelect, arrSelectArgs);
        } else {
            db.insert(DBConstants.TABLE_NAME_USER_INFO, null, values);
        }


        db.close();
        finish();
    }
}
