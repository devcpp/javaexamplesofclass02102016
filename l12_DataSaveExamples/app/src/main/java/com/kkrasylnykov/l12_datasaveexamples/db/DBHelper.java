package com.kkrasylnykov.l12_datasaveexamples.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.kkrasylnykov.l12_datasaveexamples.ToolsAndConstants.DBConstants;

public class DBHelper extends SQLiteOpenHelper {


    public DBHelper(Context context) {
        super(context, DBConstants.DB_NAME, null, DBConstants.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DBConstants.TABLE_NAME_USER_INFO +
                " (" + DBConstants.FIELD_USER_INFO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DBConstants.FIELD_USER_INFO_NAME + " TEXT NOT NULL, " +
                DBConstants.FIELD_USER_INFO_SNAME + " TEXT NOT NULL, " +
                DBConstants.FIELD_USER_INFO_PHONE + " TEXT NOT NULL, " +
                DBConstants.FIELD_USER_INFO_ADRESS + " TEXT NOT NULL);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
