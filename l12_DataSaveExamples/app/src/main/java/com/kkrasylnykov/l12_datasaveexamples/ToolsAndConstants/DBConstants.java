package com.kkrasylnykov.l12_datasaveexamples.ToolsAndConstants;

public class DBConstants {

    public static final String DB_NAME = "db_userinfo";
    public static final int DB_VERSION = 1;

    public static final String TABLE_NAME_USER_INFO = "_USER_INFO";
    public static final String FIELD_USER_INFO_ID = "_id";
    public static final String FIELD_USER_INFO_NAME = "_name";
    public static final String FIELD_USER_INFO_SNAME = "_sname";
    public static final String FIELD_USER_INFO_PHONE = "_phone";
    public static final String FIELD_USER_INFO_ADRESS = "_adress";
}
