package com.kkrasylnykov.l12_datasaveexamples.Activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kkrasylnykov.l12_datasaveexamples.R;
import com.kkrasylnykov.l12_datasaveexamples.ToolsAndConstants.AppSettings;
import com.kkrasylnykov.l12_datasaveexamples.ToolsAndConstants.DBConstants;
import com.kkrasylnykov.l12_datasaveexamples.db.DBHelper;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private final static int REQ_CODE_TERMS = 1001;

    private LinearLayout m_container = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppSettings settings = new AppSettings(this);

        m_container = (LinearLayout) findViewById(R.id.container);
        View addButton = findViewById(R.id.add);
        View removeAllButton = findViewById(R.id.removeAll);
        addButton.setOnClickListener(this);
        removeAllButton.setOnClickListener(this);

        if (!settings.isUserTermsAccepted()){
            Intent intent = new Intent(this, TermsActivity.class);
            startActivityForResult(intent, REQ_CODE_TERMS);
        }

        if (settings.isFirstStart()){
            DBHelper dbHelper = new DBHelper(this);
            settings.setIsFirstStart(false);

            SQLiteDatabase db = dbHelper.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(DBConstants.FIELD_USER_INFO_NAME, "Kos");
            values.put(DBConstants.FIELD_USER_INFO_SNAME, "Kras");
            values.put(DBConstants.FIELD_USER_INFO_PHONE, "095");
            values.put(DBConstants.FIELD_USER_INFO_ADRESS, "Kh");
            db.insert(DBConstants.TABLE_NAME_USER_INFO, null, values);

            values.clear();
            values.put(DBConstants.FIELD_USER_INFO_NAME, "Kos2");
            values.put(DBConstants.FIELD_USER_INFO_SNAME, "Kras2");
            values.put(DBConstants.FIELD_USER_INFO_PHONE, "096");
            values.put(DBConstants.FIELD_USER_INFO_ADRESS, "Kh2");
            db.insert(DBConstants.TABLE_NAME_USER_INFO, null, values);

            db.close();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        showInfo();
    }

    private void showInfo(){
        DBHelper dbHelper = new DBHelper(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(DBConstants.TABLE_NAME_USER_INFO, null, null, null, null, null, null);
        if (cursor!=null){
            m_container.removeAllViews();
            if (cursor.moveToFirst()){
                do{
                    long nID = cursor.getLong(cursor.getColumnIndex(DBConstants.FIELD_USER_INFO_ID));
                    String strName = cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_USER_INFO_NAME));
                    String strSName = cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_USER_INFO_SNAME));
                    String strPhone = cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_USER_INFO_PHONE));
                    String strAdress = cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_USER_INFO_ADRESS));

                    TextView textView = new TextView(this);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    layoutParams.setMargins(0,0,0,10);
                    textView.setLayoutParams(layoutParams);
                    textView.setText(strName + " " + strSName + "\n" + strPhone + "\n" +  strAdress);
                    textView.setOnClickListener(this);
                    textView.setTag(nID);
                    m_container.addView(textView);
                    Log.d("devcpp","nID -> " + nID);
                    Log.d("devcpp","strName -> " + strName);
                    Log.d("devcpp","strSName -> " + strSName);
                    Log.d("devcpp","strPhone -> " + strPhone);
                    Log.d("devcpp","strAdress -> " + strAdress);
                } while(cursor.moveToNext());
            }
            cursor.close();
        }
        db.close();
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode==REQ_CODE_TERMS){
            if (resultCode==RESULT_CANCELED){
                finish();
            } else if (resultCode==RESULT_OK){
                AppSettings settings = new AppSettings(this);
                settings.setIsUserTermsAccepted(true);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.add:
                startActivity(new Intent(this, EditActivity.class));
                break;
            case R.id.removeAll:
                break;
            default:
                if (v instanceof TextView){
                    long nId = (Long) v.getTag();
                    Intent intent = new Intent(this, EditActivity.class);
                    intent.putExtra(EditActivity.KEY_ID, nId);
                    startActivity(intent);
                }
                break;
        }
    }
}
