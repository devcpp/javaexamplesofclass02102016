package com.kkrasylnykov.l12_datasaveexamples.ToolsAndConstants;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class AppSettings {

    private final static String KEY_BOOLEAN_USER_TERMS_IS_ACCEPTED = "KEY_BOOLEAN_USER_TERMS_IS_ACCEPTED";
    private final static String KEY_BOOLEAN_IS_FIRST_START = "KEY_BOOLEAN_IS_FIRST_START";

    private SharedPreferences m_SharedPreferences = null;

    public  AppSettings (Context context){
        m_SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public boolean isUserTermsAccepted(){
        return m_SharedPreferences.getBoolean(KEY_BOOLEAN_USER_TERMS_IS_ACCEPTED, false);
    }

    public void setIsUserTermsAccepted(boolean bIsUserTermsAccepted){
        SharedPreferences.Editor editor = m_SharedPreferences.edit();
        editor.putBoolean(KEY_BOOLEAN_USER_TERMS_IS_ACCEPTED, bIsUserTermsAccepted);
        editor.commit();
    }

    public boolean isFirstStart(){
        return m_SharedPreferences.getBoolean(KEY_BOOLEAN_IS_FIRST_START, true);
    }

    public void setIsFirstStart(boolean bIsFirstStart){
        SharedPreferences.Editor editor = m_SharedPreferences.edit();
        editor.putBoolean(KEY_BOOLEAN_IS_FIRST_START, bIsFirstStart);
        editor.commit();
    }
}
