package l4_testlists;

import java.util.ArrayList;
import java.util.LinkedList;

public class L4_TestLists {

    public static void main(String[] args) {
        ArrayList<Integer> arrData = new ArrayList<>();
        LinkedList<Integer> arrLinkedData = new LinkedList<>();
        
        long nStartTimeArray = System.currentTimeMillis();
        for (int i=0; i<100000;i++){
            //int nPos = (int) (Math.random()*arrData.size());
            arrData.add(i);
        }
        long nEndTimeArray = System.currentTimeMillis();
        long nResArray = nEndTimeArray - nStartTimeArray;
        System.out.println("nResArray => " + nResArray);
        
        long nStartTimeLinkedArray = System.currentTimeMillis();
        for (int i=0; i<100000;i++){
            //int nPos = (int) (Math.random()*arrLinkedData.size());
            arrLinkedData.add(i);
        }
        long nEndTimeLinkedArray = System.currentTimeMillis();
        long nResLinkedArray = nEndTimeLinkedArray - nStartTimeLinkedArray;
        System.out.println("nResLinkedArray => " + nResLinkedArray);
    }
    
}
