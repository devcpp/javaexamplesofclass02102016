package com.kkrasylnykov.l20_imageloadexample.services;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DownloadService extends Service {
    public static final String ACTION = "com.kkrasylnykov.l20_imageloadexample.services.DownloadService.ACTION";

    public static final String KEY_EXTRA_URL = "KEY_EXTRA_URL";
    public static final String KEY_EXTRA_FILE_NAME = "KEY_EXTRA_FILE_NAME";

    public static final String KEY_TYPE = "KEY_TYPE";
    public static final String KEY_PROGRESS = "KEY_PROGRESS";
    public static final String KEY_URL = "KEY_URL";

    public static final int TYPE_START = 0;
    public static final int TYPE_PROGRESS = 1;
    public static final int TYPE_STOP = 2;

    ExecutorService executorService;
    @Override
    public void onCreate() {
        super.onCreate();
        executorService = Executors.newFixedThreadPool(3);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("devcpp","DownloadService -> onStartCommand -> " + startId);
        Log.d("devcpp","DownloadService -> flags -> " + flags);
        Log.d("devcpp","DownloadService -> intent -> " + intent);
        String strUrl = "";
        String strFileName = "";
        if (intent!=null){
            Bundle bundle = intent.getExtras();
            if (bundle!=null){
                strUrl = bundle.getString(KEY_EXTRA_URL, "");
                strFileName = bundle.getString(KEY_EXTRA_FILE_NAME, "");
            }
        }

        if (!strUrl.isEmpty() && !strFileName.isEmpty()){
            DownloadRunnable downloadRunnable = new DownloadRunnable(strUrl, strFileName);
            executorService.execute(downloadRunnable);
        }

        int nReturn = super.onStartCommand(intent,flags,startId);
        Log.d("devcpp","DownloadService -> nReturn -> " + nReturn);
        return nReturn;
    }

    private class DownloadRunnable implements Runnable{

        private String m_strUrl = "";
        private String m_strFileName = "";

        public DownloadRunnable(String strUrl, String strFileName){
            super();
            m_strUrl = strUrl;
            m_strFileName = strFileName;
        }

        @Override
        public void run() {
            Log.d("devcpp","start -> m_strFileName -> " + m_strFileName);
            URL url = null;
            String strTmpFIleName = "tmp_" + m_strFileName;
            File tmpfile = new File(getFilesDir() + "/" +  strTmpFIleName);
            File file = new File(getFilesDir() + "/" +  m_strFileName);
            try {
                url = new URL(m_strUrl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(1000);
                conn.setConnectTimeout(1000);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.connect();
                int response = conn.getResponseCode();
                if (response!=200){
                    return;
                }
                sendBroadcast(TYPE_START);
                /*Message message = new Message();
                Bundle bundle = new Bundle();
                bundle.putInt(KEY_TYPE, TYPE_START);
                message.setData(bundle);
                m_Handler.sendMessage(message);*/

                int contentLength = conn.getContentLength();
                Log.d("devcppSize","contentLength -> " + m_strFileName + " -> " + contentLength);
                InputStream inputStream = conn.getInputStream();
                //Создаем файл на диске
                FileOutputStream outputStream = new FileOutputStream(tmpfile);

                int bytesRead = -1;
                int nSumReadByte = 0;
                int nOldProgress = -1;
                byte[] buffer = new byte[4096];

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    nSumReadByte+=bytesRead;
                    int nProgress = (int)((((float)nSumReadByte)/contentLength)*100);
                    Log.d("devcppSize","contentLength -> " + m_strFileName + " -> " + nProgress);
                    if (nOldProgress<nProgress){
                        nOldProgress = nProgress;
                        sendBroadcast(TYPE_PROGRESS, nProgress);
                        /*message = new Message();
                        bundle = new Bundle();
                        bundle.putInt(KEY_TYPE, TYPE_PROGRESS);
                        bundle.putInt(KEY_PROGRESS, nProgress);
                        message.setData(bundle);
                        m_Handler.sendMessage(message);*/
                    }

                    outputStream.write(buffer, 0, bytesRead);
                }

                outputStream.close();
                inputStream.close();

                tmpfile.renameTo(file);
                sendBroadcast(TYPE_STOP);
                /*message = new Message();
                bundle = new Bundle();
                bundle.putInt(KEY_TYPE, TYPE_STOP);
                message.setData(bundle);
                m_Handler.sendMessage(message);*/

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.d("devcpp","stop -> m_strFileName -> " + m_strFileName);
        }

        private void sendBroadcast(int nType){
            sendBroadcast(nType,-1);
        }

        private void sendBroadcast(int nType, int nProgress){
            Intent intent = new Intent();
            intent.setAction(ACTION);
            intent.putExtra(KEY_TYPE, nType);
            intent.putExtra(KEY_URL, m_strUrl);
            if (nProgress>=0){
                intent.putExtra(KEY_PROGRESS, nProgress);
            }
            DownloadService.this.sendBroadcast(intent);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
