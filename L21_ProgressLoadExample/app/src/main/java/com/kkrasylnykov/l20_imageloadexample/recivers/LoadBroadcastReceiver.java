package com.kkrasylnykov.l20_imageloadexample.recivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.kkrasylnykov.l20_imageloadexample.services.LoadService;

public class LoadBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent serviceIntent = new Intent(context, LoadService.class);
        context.startService(serviceIntent);
    }
}
