package com.kkrasylnykov.l20_imageloadexample.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

public class LoadService extends Service {

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        for (int i=0; i<100; i++){
            Log.d("devcppLoadService", "i -> " + i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
