package com.kkrasylnykov.l20_imageloadexample.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.kkrasylnykov.l20_imageloadexample.R;
import com.kkrasylnykov.l20_imageloadexample.services.DownloadService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Formatter;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String IMAGE_URL1 = "http://wallpapersite.com/images/wallpapers/kate-upton-8000x4194-american-model-4k-8k-585.jpg";
    private static final String IMAGE_URL2 = "http://wallpapersite.com/images/wallpapers/antarctica-8320x2494-4k-8k-4302.jpg";
    private static final String IMAGE_URL3 = "http://wallpapersite.com/images/wallpapers/lucy-hale-8688x5792-venice-magazine-fall-2016-hd-4k-8k-4077.jpg";

    private static final String PROGRESS_DATA = "%s of 100";

    private ImageView imageView1 = null;
    private ImageView imageView2 = null;
    private ImageView imageView3 = null;

    private Button m_button1 = null;
    private Button m_button2 = null;
    private Button m_button3 = null;
    private TextView progressTextView = null;

    BroadcastReceiver m_ImageBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent!=null){
                Bundle bundle = intent.getExtras();
                if (bundle!=null){
                    String strUrl = bundle.getString(DownloadService.KEY_URL);
                    Button button = null;
                    switch (strUrl){
                        case IMAGE_URL1:
                            button = m_button1;
                            break;
                        case IMAGE_URL2:
                            button = m_button2;
                            break;
                        case IMAGE_URL3:
                            button = m_button3;
                            break;
                    }

                    int nType = bundle.getInt(DownloadService.KEY_TYPE, -1);
                    switch (nType){
                        case DownloadService.TYPE_START:
                            button.setEnabled(false);
                            button.setOnClickListener(null);
                            break;
                        case DownloadService.TYPE_PROGRESS:
                            int nProgress = bundle.getInt(DownloadService.KEY_PROGRESS, 0);
                            Formatter formatter = new Formatter();
                            String strProgress = formatter.format(PROGRESS_DATA, Integer.toString(nProgress)).toString();
                            button.setText(strProgress);
                            break;
                        case DownloadService.TYPE_STOP:
                            loadImage();
                            break;
                    }
                }
            }
        }
    };

    private Handler m_Handler = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView1 = (ImageView) findViewById(R.id.ImageView1);
        imageView2 = (ImageView) findViewById(R.id.ImageView2);
        imageView3 = (ImageView) findViewById(R.id.ImageView3);
        progressTextView = (TextView) findViewById(R.id.progressTextView);

        m_button1 = (Button) findViewById(R.id.Button1);
        m_button2 = (Button) findViewById(R.id.Button2);
        m_button3 = (Button) findViewById(R.id.Button3);

        loadImage();
        /*m_Handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                Bundle bundle = msg.getData();
                int nType = bundle.getInt(KEY_TYPE, -1);
                switch (nType){
                    case TYPE_START:
                        progressTextView.setVisibility(View.VISIBLE);
                        break;
                    case TYPE_PROGRESS:
                        int nProgress = bundle.getInt(KEY_PROGRESS, 0);
                        Formatter formatter = new Formatter();
                        String strProgress = formatter.format(PROGRESS_DATA, Integer.toString(nProgress)).toString();
                        progressTextView.setText(strProgress);
                        break;
                    case TYPE_STOP:
                        progressTextView.setVisibility(View.GONE);
                        loadImage();
                        break;
                }
            }
        };*/
    }

    @Override
    protected void onStart() {
        registerReceiver(m_ImageBroadcastReceiver, new IntentFilter(DownloadService.ACTION));
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(m_ImageBroadcastReceiver);
    }

    private void loadImage(){
        File file1 = new File(getFilesDir() + "/" + "image1.jpg");
        File file2 = new File(getFilesDir() + "/" + "image2.jpg");
        File file3 = new File(getFilesDir() + "/" + "image3.jpg");

        if (file1.exists()){
            m_button1.setVisibility(View.GONE);
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int nDispleyWidth = size.x;

            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;

            BitmapFactory.decodeFile(file1.getAbsolutePath(), bmOptions);

            int nFileWidth = bmOptions.outWidth;

            int nScale = (nFileWidth/nDispleyWidth);

            bmOptions = new BitmapFactory.Options();
            bmOptions.inSampleSize =nScale;
            Bitmap bmp = BitmapFactory.decodeFile(file1.getAbsolutePath(),bmOptions);
            imageView1.setImageBitmap(bmp);
        } else {
            m_button1.setOnClickListener(this);
            m_button1.setVisibility(View.VISIBLE);
        }

        if (file2.exists()){
            m_button2.setVisibility(View.GONE);
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int nDispleyWidth = size.x;

            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;

            BitmapFactory.decodeFile(file2.getAbsolutePath(), bmOptions);

            int nFileWidth = bmOptions.outWidth;

            int nScale = (nFileWidth/nDispleyWidth);

            bmOptions = new BitmapFactory.Options();
            bmOptions.inSampleSize =nScale;
            Bitmap bmp = BitmapFactory.decodeFile(file2.getAbsolutePath(),bmOptions);
            imageView2.setImageBitmap(bmp);
        } else {
            m_button2.setOnClickListener(this);
            m_button2.setVisibility(View.VISIBLE);
        }

        if (file3.exists()){
            m_button3.setVisibility(View.GONE);
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int nDispleyWidth = size.x;

            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;

            BitmapFactory.decodeFile(file3.getAbsolutePath(), bmOptions);

            int nFileWidth = bmOptions.outWidth;

            int nScale = (nFileWidth/nDispleyWidth);

            bmOptions = new BitmapFactory.Options();
            bmOptions.inSampleSize =nScale;
            Bitmap bmp = BitmapFactory.decodeFile(file3.getAbsolutePath(),bmOptions);
            imageView3.setImageBitmap(bmp);
        } else {
            m_button3.setOnClickListener(this);
            m_button3.setVisibility(View.VISIBLE);
        }

         /*else {
           new Thread(new Runnable() {
                @Override
                public void run() {
                    URL url = null;
                    try {
                        url = new URL(IMAGE_URL);
                        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                        conn.setReadTimeout(1000);
                        conn.setConnectTimeout(1000);
                        conn.setRequestMethod("GET");
                        conn.setDoInput(true);
                        conn.connect();
                        int response = conn.getResponseCode();
                        if (response!=200){
                            return;
                        }
                        Message message = new Message();
                        Bundle bundle = new Bundle();
                        bundle.putInt(KEY_TYPE, TYPE_START);
                        message.setData(bundle);
                        m_Handler.sendMessage(message);

                        int contentLength = conn.getContentLength();
                        Log.d("devcppSize","contentLength -> " + contentLength);
                        InputStream inputStream = conn.getInputStream();
                        //Создаем файл на диске
                        FileOutputStream outputStream = new FileOutputStream(file);

                        int bytesRead = -1;
                        int nSumReadByte = 0;
                        int nOldProgress = -1;
                        byte[] buffer = new byte[4096];

                        while ((bytesRead = inputStream.read(buffer)) != -1) {
                            nSumReadByte+=bytesRead;
                            int nProgress = (int)((((float)nSumReadByte)/contentLength)*100);
                            if (nOldProgress<nProgress){
                                nOldProgress = nProgress;
                                message = new Message();
                                bundle = new Bundle();
                                bundle.putInt(KEY_TYPE, TYPE_PROGRESS);
                                bundle.putInt(KEY_PROGRESS, nProgress);
                                message.setData(bundle);
                                m_Handler.sendMessage(message);
                            }

                            outputStream.write(buffer, 0, bytesRead);
                        }

                        outputStream.close();
                        inputStream.close();
                        message = new Message();
                        bundle = new Bundle();
                        bundle.putInt(KEY_TYPE, TYPE_STOP);
                        message.setData(bundle);
                        m_Handler.sendMessage(message);

                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }*/
    }

    @Override
    public void onDestroy() {
        Log.e("devcpp", "onDestroy!!!!!!!!!!!!!!!!!!");
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(this, DownloadService.class);
        switch ((view.getId())){
            case R.id.Button1:
                intent.putExtra(DownloadService.KEY_EXTRA_URL, IMAGE_URL1);
                intent.putExtra(DownloadService.KEY_EXTRA_FILE_NAME, "image1.jpg");
                break;
            case R.id.Button2:
                intent.putExtra(DownloadService.KEY_EXTRA_URL, IMAGE_URL2);
                intent.putExtra(DownloadService.KEY_EXTRA_FILE_NAME, "image2.jpg");
                break;
            case R.id.Button3:
                intent.putExtra(DownloadService.KEY_EXTRA_URL, IMAGE_URL3);
                intent.putExtra(DownloadService.KEY_EXTRA_FILE_NAME, "image3.jpg");
                break;
        }
        startService(intent);
    }
}
