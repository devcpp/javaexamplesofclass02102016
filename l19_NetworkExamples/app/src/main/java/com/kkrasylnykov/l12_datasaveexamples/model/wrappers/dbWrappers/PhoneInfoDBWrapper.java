package com.kkrasylnykov.l12_datasaveexamples.model.wrappers.dbWrappers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kkrasylnykov.l12_datasaveexamples.db.DBHelper;
import com.kkrasylnykov.l12_datasaveexamples.model.PhoneInfo;
import com.kkrasylnykov.l12_datasaveexamples.model.UserInfo;
import com.kkrasylnykov.l12_datasaveexamples.toolsAndConstants.DBConstants;

import java.util.ArrayList;

public class PhoneInfoDBWrapper {

    private DBHelper m_dbHelper = null;
    private String m_strTableName = "";

    public PhoneInfoDBWrapper(Context context){
        m_dbHelper = new DBHelper(context);
        m_strTableName = DBConstants.TABLE_NAME_PHONES;
    }

    public ArrayList<PhoneInfo> getAll(){
        ArrayList<PhoneInfo> arrResult = new ArrayList<>();
        SQLiteDatabase db = m_dbHelper.getReadableDatabase();
        Cursor cursor = db.query(m_strTableName,null,null,null,null,null,null);
        if (cursor!=null){
            if(cursor.moveToFirst()){
                do {
                    arrResult.add(new PhoneInfo(cursor));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        db.close();
        return arrResult;
    }

    public ArrayList<PhoneInfo> getAllByUserId(long nUserId){
        ArrayList<PhoneInfo> arrResult = new ArrayList<>();
        SQLiteDatabase db = m_dbHelper.getReadableDatabase();
        String strSelect = DBConstants.FIELD_PHONES_USER_ID + "=?";
        String[] arrArg = new String[]{Long.toString(nUserId)};
        Cursor cursor = db.query(m_strTableName,null,strSelect,arrArg,null,null,null);
        if (cursor!=null){
            if(cursor.moveToFirst()){
                do {
                    arrResult.add(new PhoneInfo(cursor));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        db.close();
        return arrResult;
    }

    public PhoneInfo getUserById(long nId){
        PhoneInfo result = null;
        SQLiteDatabase db = m_dbHelper.getReadableDatabase();
        String strSelect = DBConstants.FIELD_PHONES_ID + "=?";
        String[] arrArg = new String[]{Long.toString(nId)};
        Cursor cursor = db.query(m_strTableName,null,strSelect,arrArg,null,null,null);
        if (cursor!=null){
            if(cursor.moveToFirst()){
                result = new PhoneInfo(cursor);
            }
            cursor.close();
        }
        db.close();
        return result;
    }

    public boolean updatePhone(PhoneInfo item){
        SQLiteDatabase db = m_dbHelper.getWritableDatabase();
        String strSelect = DBConstants.FIELD_PHONES_ID + "=?";
        String[] arrArg = new String[]{Long.toString(item.getId())};
        int nCountUdate = db.update(m_strTableName,item.getContentValues(),strSelect, arrArg);
        db.close();
        return nCountUdate==1;
    }

    public boolean insertPhone(PhoneInfo item){
        SQLiteDatabase db = m_dbHelper.getWritableDatabase();
        long nId = db.insert(m_strTableName,null,item.getContentValues());
        db.close();
        return nId>=0;
    }

    public boolean removePhone(PhoneInfo item){
        SQLiteDatabase db = m_dbHelper.getWritableDatabase();
        String strSelect = DBConstants.FIELD_PHONES_ID + "=?";
        String[] arrArg = new String[]{Long.toString(item.getId())};
        int nCountUdate = db.delete(m_strTableName,strSelect, arrArg);
        db.close();
        return nCountUdate==1;
    }

    public boolean removeAll(){
        SQLiteDatabase db = m_dbHelper.getWritableDatabase();
        int nCountUdate = db.delete(m_strTableName,null, null);
        db.close();
        return nCountUdate>0;
    }
}
