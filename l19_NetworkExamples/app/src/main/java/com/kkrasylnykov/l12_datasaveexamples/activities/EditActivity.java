package com.kkrasylnykov.l12_datasaveexamples.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kkrasylnykov.l12_datasaveexamples.R;
import com.kkrasylnykov.l12_datasaveexamples.model.PhoneInfo;
import com.kkrasylnykov.l12_datasaveexamples.model.UserInfo;
import com.kkrasylnykov.l12_datasaveexamples.model.engines.UserInfoEngine;

import java.util.ArrayList;

public class EditActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String KEY_ID = "KEY_ID";

    private EditText m_nameEditText = null;
    private EditText m_snameEditText = null;
    private EditText m_phoneEditText = null;
    private EditText m_adressEditText = null;

    private UserInfo m_userInfo = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        m_nameEditText = (EditText) findViewById(R.id.name);
        m_snameEditText = (EditText) findViewById(R.id.sname);
        m_phoneEditText = (EditText) findViewById(R.id.phone);
        m_adressEditText = (EditText) findViewById(R.id.adress);

        long nId = -1;
        Intent intent = getIntent();
        if (intent!=null){
            Bundle bundle = intent.getExtras();
            if (bundle!=null){
                nId = bundle.getLong(KEY_ID, -1);
            }
        }

        Button addView = (Button) findViewById(R.id.addItem);
        addView.setOnClickListener(this);

        if (nId>-1){
            UserInfoEngine engine = new UserInfoEngine(this);
            m_userInfo = engine.getUserById(nId);

            if (m_userInfo==null){
                Toast.makeText(this,"User not found", Toast.LENGTH_LONG).show();
                finish();
            }
            Button delView = (Button) findViewById(R.id.delItem);
            delView.setVisibility(View.VISIBLE);
            delView.setOnClickListener(this);

            m_nameEditText.setText(m_userInfo.getName());
            m_snameEditText.setText(m_userInfo.getSName());
            //m_phoneEditText.setText(m_userInfo.getPhone());
            m_adressEditText.setText(m_userInfo.getAddress());
            addView.setText("Update");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.addItem:
                addItem();
                break;
            case R.id.delItem:
                delItem();
                break;
        }
    }

    private void delItem(){
        UserInfoEngine engine = new UserInfoEngine(this);
        engine.removeUser(m_userInfo);
        finish();
    }

    private void addItem(){
        if (m_userInfo==null){
            m_userInfo = new UserInfo();
        }

        m_userInfo.setName(m_nameEditText.getText().toString());
        m_userInfo.setSName(m_snameEditText.getText().toString());
        ArrayList<PhoneInfo> arrPhones = new ArrayList<>();
        arrPhones.add(new PhoneInfo(m_phoneEditText.getText().toString()));
        m_userInfo.setPhones(arrPhones);
        m_userInfo.setAddress(m_adressEditText.getText().toString());

        new Thread(new Runnable() {
            @Override
            public void run() {
                UserInfoEngine engine = new UserInfoEngine(EditActivity.this);
                if (m_userInfo.getId()>-1){
                    engine.updateUser(m_userInfo);
                } else {
                    engine.insertUser(m_userInfo);
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                });

            }
        }).start();

    }
}
