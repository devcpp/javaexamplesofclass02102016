package com.kkrasylnykov.l12_datasaveexamples.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ListView;

import com.kkrasylnykov.l12_datasaveexamples.R;
import com.kkrasylnykov.l12_datasaveexamples.adapters.UserInfoExpandableListAdapter;
import com.kkrasylnykov.l12_datasaveexamples.adapters.UserInfoRecyclerAdapter;
import com.kkrasylnykov.l12_datasaveexamples.model.TmpModel;
import com.kkrasylnykov.l12_datasaveexamples.toolsAndConstants.AppSettings;
import com.kkrasylnykov.l12_datasaveexamples.model.UserInfo;
import com.kkrasylnykov.l12_datasaveexamples.model.engines.UserInfoEngine;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, UserInfoRecyclerAdapter.OnClickUserInfoListener {

    private final static int REQ_CODE_TERMS = 1001;

    private ExpandableListView m_expandableListView = null;
    private UserInfoExpandableListAdapter m_adapter = null;
    private ArrayList<UserInfo> m_arrData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppSettings settings = new AppSettings(this);

        m_expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);
        m_adapter = new UserInfoExpandableListAdapter(m_arrData);
        m_expandableListView.setAdapter(m_adapter);

        View addButton = findViewById(R.id.add);
        View removeAllButton = findViewById(R.id.removeAll);
        addButton.setOnClickListener(this);
        removeAllButton.setOnClickListener(this);

        if (!settings.isUserTermsAccepted()){
            Intent intent = new Intent(this, TermsActivity.class);
            startActivityForResult(intent, REQ_CODE_TERMS);
        }

        if (settings.isFirstStart()){
            settings.setIsFirstStart(false);

            /*UserInfoEngine engine = new UserInfoEngine(this);
            for (int i=0; i<500; i++){
                UserInfo userInfo = new UserInfo();
                userInfo.setName("test"+i);
                userInfo.setSName("test_"+i);
                userInfo.setPhone("095"+i);
                userInfo.setAddress("adr"+i);
                engine.insertUser(userInfo);
            }*/

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        showInfo();
    }

    private void showInfo(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                UserInfoEngine engine = new UserInfoEngine(MainActivity.this);
                ArrayList<UserInfo> arrData = engine.getAll();
                m_arrData.clear();
                m_arrData.addAll(arrData);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        m_adapter.notifyDataSetChanged();
                    }
                });
            }
        }).start();
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode==REQ_CODE_TERMS){
            if (resultCode==RESULT_CANCELED){
                finish();
            } else if (resultCode==RESULT_OK){
                AppSettings settings = new AppSettings(this);
                settings.setIsUserTermsAccepted(true);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.add:
                startActivity(new Intent(this, EditActivity.class));
                break;
            case R.id.removeAll:
                removeAllItems();
                break;
        }
    }

    private void removeAllItems(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                UserInfoEngine engine = new UserInfoEngine(MainActivity.this);
                engine.removeAll();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showInfo();
                    }
                });

            }
        }).start();

    }

    @Override
    public void onClickUserInfo(View view, long nId) {
        Intent intent = new Intent(this, EditActivity.class);
        intent.putExtra(EditActivity.KEY_ID, nId);
        startActivity(intent);
    }
}
