package com.kkrasylnykov.l12_datasaveexamples.model;

public class TmpModel {

    private String m_strName = "";
    private String m_strPostCode = "";

    public TmpModel(String m_strName, String m_strPOstCode) {
        this.m_strName = m_strName;
        this.m_strPostCode = m_strPOstCode;
    }

    public String getName() {
        return m_strName;
    }

    public String getPostCode() {
        return m_strPostCode;
    }
}
