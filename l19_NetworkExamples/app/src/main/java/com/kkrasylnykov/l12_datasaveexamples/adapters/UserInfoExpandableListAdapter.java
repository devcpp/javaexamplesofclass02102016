package com.kkrasylnykov.l12_datasaveexamples.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.kkrasylnykov.l12_datasaveexamples.model.UserInfo;

import java.util.ArrayList;

public class UserInfoExpandableListAdapter extends BaseExpandableListAdapter {

    private  ArrayList<UserInfo> m_arrData = null;

    public UserInfoExpandableListAdapter(ArrayList<UserInfo> arrData){
        m_arrData = arrData;
    }


    @Override
    public int getGroupCount() {
        return m_arrData.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return m_arrData.get(groupPosition).getPhones().size() + 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return m_arrData.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return m_arrData.get(groupPosition).getPhones().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return m_arrData.get(groupPosition).getId();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return m_arrData.get(groupPosition).getPhones().get(childPosition).getId();
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if(convertView==null){
            LayoutInflater li = (LayoutInflater)parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(android.R.layout.simple_expandable_list_item_1, parent, false);
        }
        TextView tv = (TextView) convertView.findViewById(android.R.id.text1);
        tv.setText(m_arrData.get(groupPosition).getName()+" "+m_arrData.get(groupPosition).getSName());
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView==null){
            LayoutInflater li = (LayoutInflater)parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(android.R.layout.simple_list_item_1, parent, false);
        }
        TextView tv = (TextView) convertView.findViewById(android.R.id.text1);
        if (isLastChild){
            tv.setText(m_arrData.get(groupPosition).getAddress());
        } else {
            tv.setText(m_arrData.get(groupPosition).getPhones().get(childPosition).getPhone());
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
