package com.kkrasylnykov.l12_datasaveexamples.model.engines;

import android.content.Context;

import com.kkrasylnykov.l12_datasaveexamples.model.PhoneInfo;
import com.kkrasylnykov.l12_datasaveexamples.model.UserInfo;
import com.kkrasylnykov.l12_datasaveexamples.model.wrappers.dbWrappers.PhoneInfoDBWrapper;
import com.kkrasylnykov.l12_datasaveexamples.model.wrappers.dbWrappers.UserInfoDBWrapper;
import com.kkrasylnykov.l12_datasaveexamples.model.wrappers.networkWrappers.UserInfoNetworkWrapper;

import java.util.ArrayList;

public class UserInfoEngine {

    private Context m_Context = null;

    public UserInfoEngine(Context context){
        m_Context  = context;
    }


    public ArrayList<UserInfo> getAll(){
        UserInfoDBWrapper dbWrapper = new UserInfoDBWrapper(m_Context);
        ArrayList<UserInfo> arrData = dbWrapper.getAll();
        PhoneInfoDBWrapper phoneInfoDBWrapper = new PhoneInfoDBWrapper(m_Context);
        for (UserInfo userInfo:arrData){
            userInfo.setPhones(phoneInfoDBWrapper.getAllByUserId(userInfo.getId()));
        }

        if (arrData.size()==0){
            UserInfoNetworkWrapper networkWrapper = new UserInfoNetworkWrapper(m_Context);
            arrData = networkWrapper.getAll();

            for (UserInfo userInfo:arrData){
                insertUser(userInfo);
            }
        }
        return arrData;
    }

    public UserInfo getUserById(long nId){
        UserInfoDBWrapper dbWrapper = new UserInfoDBWrapper(m_Context);
        UserInfo userInfo = dbWrapper.getUserById(nId);
        PhoneInfoDBWrapper phoneInfoDBWrapper = new PhoneInfoDBWrapper(m_Context);
        userInfo.setPhones(phoneInfoDBWrapper.getAllByUserId(userInfo.getId()));
        return userInfo;
    }

    public boolean updateUser(UserInfo item){
        UserInfoDBWrapper dbWrapper = new UserInfoDBWrapper(m_Context);
        boolean bResult = dbWrapper.updateUser(item);
        PhoneInfoDBWrapper phoneInfoDBWrapper = new PhoneInfoDBWrapper(m_Context);
        ArrayList<PhoneInfo> arrDataPhones = item.getPhones();
        for (PhoneInfo phoneInfo:arrDataPhones){
            if (phoneInfo.getUserId()==-1){
                phoneInfo.setUserId(item.getId());
                phoneInfoDBWrapper.insertPhone(phoneInfo);
            } else {
                phoneInfoDBWrapper.updatePhone(phoneInfo);
            }
        }
        return bResult;
    }

    public boolean insertUser(UserInfo item){
        if (item.getServerId()==-1){
            UserInfoNetworkWrapper networkWrapper = new UserInfoNetworkWrapper(m_Context);
            networkWrapper.insertUser(item);
        }

        UserInfoDBWrapper dbWrapper = new UserInfoDBWrapper(m_Context);
        long nUserId = dbWrapper.insertUser(item);
        PhoneInfoDBWrapper phoneInfoDBWrapper = new PhoneInfoDBWrapper(m_Context);
        ArrayList<PhoneInfo> arrDataPhones = item.getPhones();
        if (arrDataPhones!=null){
            for (PhoneInfo phoneInfo:arrDataPhones){
                phoneInfo.setUserId(nUserId);
                phoneInfoDBWrapper.insertPhone(phoneInfo);
            }
        }
        return nUserId>=0;
    }

    public boolean removeUser(UserInfo item){
        PhoneInfoDBWrapper phoneInfoDBWrapper = new PhoneInfoDBWrapper(m_Context);
        ArrayList<PhoneInfo> arrDataPhones = item.getPhones();
        for (PhoneInfo phoneInfo:arrDataPhones){
            phoneInfoDBWrapper.removePhone(phoneInfo);
        }
        UserInfoDBWrapper dbWrapper = new UserInfoDBWrapper(m_Context);
        return dbWrapper.removeUser(item);
    }

    public boolean removeAll(){
        ArrayList<UserInfo> arrData =  getAll();
        UserInfoNetworkWrapper networkWrapper = new UserInfoNetworkWrapper(m_Context);
        for (UserInfo user:arrData){
            networkWrapper.removeUser(user);
        }

        PhoneInfoDBWrapper phoneInfoDBWrapper = new PhoneInfoDBWrapper(m_Context);
        phoneInfoDBWrapper.removeAll();
        UserInfoDBWrapper dbWrapper = new UserInfoDBWrapper(m_Context);
        return dbWrapper.removeAll();

    }
}
