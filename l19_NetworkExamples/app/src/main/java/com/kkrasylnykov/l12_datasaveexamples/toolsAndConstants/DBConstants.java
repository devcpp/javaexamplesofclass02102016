package com.kkrasylnykov.l12_datasaveexamples.toolsAndConstants;

public class DBConstants {

    public static final String DB_NAME = "db_userinfo";
    public static final int DB_VERSION = 2;

    public static final String TABLE_NAME_USER_INFO = "_USER_INFO";
    public static final String FIELD_USER_INFO_ID = "_id";
    public static final String FIELD_USER_INFO_SERVER_ID = "_server_id";
    public static final String FIELD_USER_INFO_NAME = "_name";
    public static final String FIELD_USER_INFO_SNAME = "_sname";
    public static final String FIELD_USER_INFO_ADRESS = "_adress";
    public static final String FIELD_OLD_1_USER_INFO_PHONE = "_phone";

    public static final String TABLE_NAME_PHONES = "_PHONES";
    public static final String FIELD_PHONES_ID = "_id";
    public static final String FIELD_PHONES_USER_ID = "_user_id";
    public static final String FIELD_PHONES_PHONE = "_phone";

}
