package com.kkrasylnykov.l12_datasaveexamples.model.wrappers.networkWrappers;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.kkrasylnykov.l12_datasaveexamples.model.UserInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Formatter;

public class UserInfoNetworkWrapper {
    private static final String BASE_URL = "http://xutpuk.pp.ua";

    private static final String GET_ALL_USER_URL = BASE_URL + "/api/users.json?_format=json";
    private static final String REMOVE_USER = BASE_URL + "/api/users/%s.json?_format=json";

    private static final String TYPE_REQUEST_GET = "GET";
    private static final String TYPE_REQUEST_POST = "POST";
    private static final String TYPE_REQUEST_DELETE = "DELETE";

    private static final int RESPONCE_CODE_OK = 200;


    private int m_nResponceCode = -1;
    private String m_strResponse = "";
    private Context m_context = null;

    public UserInfoNetworkWrapper(Context context){
        m_context = context;
    }

    public ArrayList<UserInfo> getAll(){
        ArrayList<UserInfo> arrData = new ArrayList<>();
        if (sendRequest(GET_ALL_USER_URL, null)){
            Log.d("devcppNetwork","Response -> " + m_strResponse);
            try {
                JSONArray arrJsonUserInfo = new JSONArray(m_strResponse);
                int nCount = arrJsonUserInfo.length();
                for (int i=0; i<nCount; i++){
                    JSONObject jsonUserInfo = arrJsonUserInfo.getJSONObject(i);
                    arrData.add(new UserInfo(jsonUserInfo));
                }
            } catch (final JSONException e) {
                e.printStackTrace();
                ((Activity)m_context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(m_context, "JSONException!!!! " + e.toString(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        } else {
            ((Activity)m_context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(m_context, "Network Error!!!!", Toast.LENGTH_LONG).show();
                }
            });
        }
        return arrData;
    }

    public void insertUser(UserInfo item){
        try {
            JSONObject jsonObject = item.getJSONObject();

            if (sendRequest(GET_ALL_USER_URL, jsonObject.toString())){
                JSONObject jsonResponse = new JSONObject(m_strResponse);
                item.setServerId(jsonResponse.getLong("id"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void removeUser(UserInfo item){
        Formatter formatter = new Formatter();
        String strUrl = formatter.format(REMOVE_USER,Long.toString(item.getServerId())).toString();
        sendRequest(strUrl,null,TYPE_REQUEST_DELETE);
        Log.d("devcppNetwork","removeUser -> strUrl -> " + strUrl);
    }

    private boolean sendRequest(String strUrl, String strBody){
        return sendRequest(strUrl, strBody, strBody!=null?TYPE_REQUEST_POST:TYPE_REQUEST_GET);
    }

    private boolean sendRequest(String strUrl, String strBody, String strType){
        boolean bResult = false;

        Log.d("devcppNetwork", "sendRequest -> strUrl -> " + strUrl);
        Log.d("devcppNetwork", "sendRequest -> strBody -> " + strBody);

        try {
            m_nResponceCode = -1;
            m_strResponse = "";

            URL url = new URL(strUrl);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(10000);
            connection.setRequestMethod(strType);

            connection.setRequestProperty( "Content-Type", "application/json");
            connection.setRequestProperty( "charset", "utf-8");

            if (strType.equals(TYPE_REQUEST_POST)) {
                byte[] postData = strBody.getBytes(StandardCharsets.UTF_8);
                connection.getOutputStream().write(postData);
            }

            connection.connect();

            m_nResponceCode = connection.getResponseCode();

            if (m_nResponceCode ==RESPONCE_CODE_OK){
                InputStream is = connection.getInputStream();

                if (is!=null){
                    BufferedReader reader = null;
                    reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));

                    if (reader!=null){
                        StringBuilder stringBuilder = new StringBuilder();
                        while (true){
                            String tempStr = reader.readLine();
                            if(tempStr != null){
                                stringBuilder.append(tempStr);
                            } else {
                                m_strResponse = stringBuilder.toString();
                                break;
                            }
                        }
                    }
                }
                bResult = true;
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bResult;
    }
}
