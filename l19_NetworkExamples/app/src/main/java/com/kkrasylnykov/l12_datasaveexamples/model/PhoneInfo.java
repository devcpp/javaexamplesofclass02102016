package com.kkrasylnykov.l12_datasaveexamples.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.kkrasylnykov.l12_datasaveexamples.toolsAndConstants.DBConstants;

import org.json.JSONObject;

public class PhoneInfo {

    private long m_nId = -1;
    private long m_nUserId = -1;
    private String m_strPhone = "";

    public PhoneInfo(){
    }

    public PhoneInfo(Cursor cursor){
        this.m_nId = cursor.getLong(cursor.getColumnIndex(DBConstants.FIELD_PHONES_ID));
        this.m_nUserId = cursor.getLong(cursor.getColumnIndex(DBConstants.FIELD_PHONES_USER_ID));
        this.m_strPhone = cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_PHONES_PHONE));
    }

    public PhoneInfo(String strPhone){
        this.m_nId = -1;
        this.m_nUserId = -1;
        this.m_strPhone = strPhone;
    }

    public long getId() {
        return m_nId;
    }

    public void setId(long m_nId) {
        this.m_nId = m_nId;
    }

    public long getUserId() {
        return m_nUserId;
    }

    public void setUserId(long m_nUserId) {
        this.m_nUserId = m_nUserId;
    }

    public String getPhone() {
        return m_strPhone;
    }

    public void setPhone(String m_strPhone) {
        this.m_strPhone = m_strPhone;
    }

    public ContentValues getContentValues(){
        ContentValues values = new ContentValues();
        values.put(DBConstants.FIELD_PHONES_USER_ID, getUserId());
        values.put(DBConstants.FIELD_PHONES_PHONE, getPhone());
        return values;
    }
}
