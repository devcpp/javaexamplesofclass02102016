package com.kkrasylnykov.l12_datasaveexamples.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.kkrasylnykov.l12_datasaveexamples.toolsAndConstants.DBConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class UserInfo {

    private long m_nId = -1;
    private long m_nServerId = -1;
    private String m_strName = "";
    private String m_strSName = "";
    private String m_strAddress = "";

    private ArrayList<PhoneInfo> m_arrPhones = null;

    public UserInfo(){

    }

    public UserInfo(String strName, String strSName, String strAddress) {
        this.m_strName = strName;
        this.m_strSName = strSName;
        this.m_strAddress = strAddress;
    }

    public UserInfo(Cursor cursor){
        this.m_nId = cursor.getLong(cursor.getColumnIndex(DBConstants.FIELD_USER_INFO_ID));
        this.m_nServerId = cursor.getLong(cursor.getColumnIndex(DBConstants.FIELD_USER_INFO_SERVER_ID));
        this.m_strName = cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_USER_INFO_NAME));
        this.m_strSName = cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_USER_INFO_SNAME));
        this.m_strAddress = cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_USER_INFO_ADRESS));

    }

    public UserInfo(JSONObject jsonUserInfo) throws JSONException {
        this.m_nId = -1;
        this.m_nServerId = jsonUserInfo.getInt("id");
        this.m_strName = jsonUserInfo.getString("name");
        this.m_strSName = jsonUserInfo.getString("sname");
        JSONArray arrJsonPhones = jsonUserInfo.getJSONArray("phones");
        int nPhonesCount = arrJsonPhones.length();
        if (nPhonesCount>0){
            m_arrPhones = new ArrayList<>();
        }
        for (int i=0;i<nPhonesCount;i++){
            String strPhone = arrJsonPhones.getString(i);
            m_arrPhones.add(new PhoneInfo(strPhone));
        }

        JSONArray arrJsonAddress = jsonUserInfo.getJSONArray("address");
        if (arrJsonAddress.length()>0){
            this.m_strAddress = arrJsonAddress.getString(0);
        }

    }

    public long getId() {
        return m_nId;
    }

    public void setId(long m_nId) {
        this.m_nId = m_nId;
    }

    public long getServerId() {
        return m_nServerId;
    }

    public void setServerId(long m_nServerId) {
        this.m_nServerId = m_nServerId;
    }

    public String getName() {
        return m_strName;
    }

    public void setName(String m_strName) {
        this.m_strName = m_strName;
    }

    public String getSName() {
        return m_strSName;
    }

    public void setSName(String m_strSName) {
        this.m_strSName = m_strSName;
    }

    public ArrayList<PhoneInfo> getPhones() {
        return m_arrPhones;
    }

    public void setPhones(ArrayList<PhoneInfo> arrPhones) {
        this.m_arrPhones = arrPhones;
    }

    public String getAddress() {
        return m_strAddress;
    }

    public void setAddress(String m_strAddress) {
        this.m_strAddress = m_strAddress;
    }

    public JSONObject getJSONObject() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name",getName());
        jsonObject.put("sname",getSName());

        if (getPhones()!=null){
            JSONArray jsonArrayPnones = new JSONArray();
            for (PhoneInfo phoneInfo:getPhones()){
                jsonArrayPnones.put(phoneInfo.getPhone());
            }
            jsonObject.put("phones",jsonArrayPnones);
        }

        JSONArray jsonArrayAddress = new JSONArray();
        jsonArrayAddress.put(getAddress());
        jsonObject.put("address",jsonArrayAddress);
        return jsonObject;
    }

    public ContentValues getContentValues(){
        ContentValues values = new ContentValues();
        values.put(DBConstants.FIELD_USER_INFO_SERVER_ID, getServerId());
        values.put(DBConstants.FIELD_USER_INFO_NAME, getName());
        values.put(DBConstants.FIELD_USER_INFO_SNAME, getSName());
        values.put(DBConstants.FIELD_USER_INFO_ADRESS, getAddress());
        return values;
    }
}
