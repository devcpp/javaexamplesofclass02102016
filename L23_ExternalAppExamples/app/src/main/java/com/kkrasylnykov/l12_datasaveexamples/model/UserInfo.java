package com.kkrasylnykov.l12_datasaveexamples.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.kkrasylnykov.l12_datasaveexamples.toolsAndConstants.DBConstants;

public class UserInfo {

    private long m_nId = -1;
    private String m_strName = "";
    private String m_strSName = "";
    private String m_strPhone = "";
    private String m_strAddress = "";

    public UserInfo(){

    }

    public UserInfo(String strName, String strSName, String strPhone, String strAddress) {
        this.m_strName = strName;
        this.m_strSName = strSName;
        this.m_strPhone = strPhone;
        this.m_strAddress = strAddress;
    }

    public UserInfo(Cursor cursor){
        this.m_nId = cursor.getLong(cursor.getColumnIndex(DBConstants.FIELD_USER_INFO_ID));
        this.m_strName = cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_USER_INFO_NAME));
        this.m_strSName = cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_USER_INFO_SNAME));
        this.m_strPhone = cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_USER_INFO_PHONE));
        this.m_strAddress = cursor.getString(cursor.getColumnIndex(DBConstants.FIELD_USER_INFO_ADRESS));

    }

    public long getId() {
        return m_nId;
    }

    public void setId(long m_nId) {
        this.m_nId = m_nId;
    }

    public String getName() {
        return m_strName;
    }

    public void setName(String m_strName) {
        this.m_strName = m_strName;
    }

    public String getSName() {
        return m_strSName;
    }

    public void setSName(String m_strSName) {
        this.m_strSName = m_strSName;
    }

    public String getPhone() {
        return m_strPhone;
    }

    public void setPhone(String m_strPhone) {
        this.m_strPhone = m_strPhone;
    }

    public String getAddress() {
        return m_strAddress;
    }

    public void setAddress(String m_strAddress) {
        this.m_strAddress = m_strAddress;
    }

    public ContentValues getContentValues(){
        ContentValues values = new ContentValues();
        values.put(DBConstants.FIELD_USER_INFO_NAME, getName());
        values.put(DBConstants.FIELD_USER_INFO_SNAME, getSName());
        values.put(DBConstants.FIELD_USER_INFO_PHONE, getPhone());
        values.put(DBConstants.FIELD_USER_INFO_ADRESS, getAddress());
        return values;
    }
}
