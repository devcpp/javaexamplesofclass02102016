package com.kkrasylnykov.l12_datasaveexamples.contentProveder;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.Nullable;

import com.kkrasylnykov.l12_datasaveexamples.db.DBHelper;
import com.kkrasylnykov.l12_datasaveexamples.toolsAndConstants.DBConstants;

public class DBContentProvider extends ContentProvider {

    private static final String AUTHORITY = "com.kkrasylnykov.l12_datasaveexamples.contentProveder.DBContentProvider.FFFC48566F9F1";

    public static final String USER_INFO     = DBConstants.TABLE_NAME_USER_INFO;

    private static final int URI_USER_INFO      = 1;

    private static final UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, USER_INFO, URI_USER_INFO);
    }

    DBHelper dbHelper = null;

    @Override
    public boolean onCreate() {
        dbHelper = new DBHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor result = null;
        switch (uriMatcher.match(uri)){
            case URI_USER_INFO:
                result = dbHelper.getReadableDatabase().query(USER_INFO,projection, selection, selectionArgs,null,null,sortOrder);
                break;
        }
        return result;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        switch (uriMatcher.match(uri)){
            case URI_USER_INFO:
                dbHelper.getWritableDatabase().insert(USER_INFO,null,values);
                break;
        }
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        switch (uriMatcher.match(uri)){
            case URI_USER_INFO:
                dbHelper.getWritableDatabase().delete(USER_INFO,selection,selectionArgs);
                break;
        }
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        switch (uriMatcher.match(uri)){
            case URI_USER_INFO:
                dbHelper.getWritableDatabase().update(USER_INFO,values,selection,selectionArgs);
                break;
        }
        return 0;
    }
}
