package com.kkrasylnykov.l20_imageloadexample;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private static final String IMAGE_URL = "https://pp.vk.me/c629301/v629301153/32783/R9ztnRMyCAM.jpg";

    private ImageView imageView = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = (ImageView) findViewById(R.id.ImageView);

        findViewById(R.id.Button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadImage();
            }
        });
    }

    private void loadImage(){
        final File file = new File(getFilesDir() + "/" + "image.jpg");
        if (file.exists()){
            Bitmap bmp = BitmapFactory.decodeFile(file.getAbsolutePath(),new BitmapFactory.Options());
            imageView.setImageBitmap(bmp);
        } else {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    URL url = null;
                    try {
                        url = new URL(IMAGE_URL);
                        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                        conn.setReadTimeout(1000);
                        conn.setConnectTimeout(1000);
                        conn.setRequestMethod("GET");
                        conn.setDoInput(true);
                        conn.connect();
                        int response = conn.getResponseCode();
                        if (response!=200){
                            return;
                        }
                        int contentLength = conn.getContentLength();
                        InputStream inputStream = conn.getInputStream();
                        //Создаем файл на диске
                        FileOutputStream outputStream = new FileOutputStream(file);

                        int bytesRead = -1;
                        byte[] buffer = new byte[4096];

                        while ((bytesRead = inputStream.read(buffer)) != -1) {
                            outputStream.write(buffer, 0, bytesRead);
                        }

                        outputStream.close();
                        inputStream.close();

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loadImage();
                            }
                        });

                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }



    }
}
