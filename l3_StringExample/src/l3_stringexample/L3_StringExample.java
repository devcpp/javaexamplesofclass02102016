package l3_stringexample;

import java.util.Scanner;

public class L3_StringExample {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String str1 = scan.nextLine();
        int  nLength = str1.length();
        System.out.println("Length -> " + nLength);
        
        int nPost1 = str1.indexOf("next");
        System.out.println("nPost1 -> " + nPost1);
        
        int nPost2 = str1.indexOf("next", nPost1+1);
        System.out.println("nPost2 -> " + nPost2);
        
        int nPost3 = str1.lastIndexOf("next");
        System.out.println("nPost3 -> " + nPost3);
        
        int nPost4 = str1.lastIndexOf("next", nLength-10);
        System.out.println("nPost4 -> " + nPost4);
        
        String str2 = str1.replace("next", "prev");
        System.out.println("str2 -> " + str2);
        
        String str3 = str1.substring(nPost1);
        System.out.println("str3 -> " + str3);
        
        String str4 = str1.substring((nPost1+"next".length()),nPost3);
        System.out.println("str4 -> " + str4);
    }
    
}
