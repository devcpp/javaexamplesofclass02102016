package com.kkrasylnykov.l17_fileandpermissionexample.adapters;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class FileListViewAdapter extends BaseAdapter {

    private ArrayList<String> m_arrData = null;

    public FileListViewAdapter(ArrayList<String> arrData){
        m_arrData = arrData;
    }
    @Override
    public int getCount() {
        return m_arrData.size();
    }
    @Override
    public Object getItem(int i) {
        return m_arrData.get(i);
    }
    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view==null){
            view = new TextView(viewGroup.getContext());
        }
        ((TextView)view).setText(m_arrData.get(i));
        return view;
    }
}
