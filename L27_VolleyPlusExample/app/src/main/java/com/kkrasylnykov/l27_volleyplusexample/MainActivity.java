package com.kkrasylnykov.l27_volleyplusexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.GsonRequest;
import com.android.volley.toolbox.Volley;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RequestQueue mRequestQueue = Volley.newRequestQueue(this);
        GsonRequest<UserInfo[]> request = new GsonRequest<UserInfo[]>(Request.Method.GET,
                "http://xutpuk.pp.ua/api/users.json", UserInfo[].class, null, null,
                new Response.Listener<UserInfo[]>() {
                    @Override
                    public void onResponse(UserInfo[] response) {
                        Log.d("devcpp", "response -> " + response[0].getId());
                        Log.d("devcpp", "response -> " + response[0].getName());
                        Log.d("devcpp", "response -> " + response[0].getSname());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("devcpp", "onErrorResponse -> " + error.toString());
                    }
        });
        mRequestQueue.add(request);
    }
}
