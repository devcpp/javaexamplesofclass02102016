package com.kkrasylnykov.l18_syncexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    Bank bank = new Bank();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Thread client1 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int n=0;n<500000; n++){
                    bank.addOneDollar();
                }
                Log.d("devcpp", "client1 -> " + bank.getCash());
            }
        });
        client1.start();

        Thread client2 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int n=0;n<500000; n++){
                    bank.getOneDollar();
                }
                Log.d("devcpp", "client2 -> " + bank.getCash());
            }
        });
        client2.start();
    }

    private class Bank{

        private int n_cash = 0;
        private Object object = new Object();

        public synchronized void addOneDollar(){
            n_cash = n_cash + 1;
        }

        public void getOneDollar(){
            synchronized (Bank.class){
                n_cash = n_cash - 1;
            }
        }

        public synchronized int getCash(){
            return n_cash;
        }
    }


}
