package l4_mapexample;

import java.util.HashMap;

public class L4_MapExample {
    public static void main(String[] args) {
        HashMap<String, String> mapData = new HashMap<>();
        
        mapData.put("Name", "Kos");
        mapData.put("SName", "Kras");
        mapData.put("Age", "28");
        mapData.put("Phone", "+38095");
        mapData.put("Name", "Kos007");
        
        System.out.println("mapData.size() => " + mapData.size());
        
        for (String strVal:mapData.keySet()){
            System.out.println("mapData.get(" + strVal + ") -> " + mapData.get(strVal));
        }
        
        for (String strVal:mapData.values()){
            System.out.println("strVal -> " +  strVal);
        }
        
        System.out.println("mapData.replace(\"Age\", \"28\", \"45\") -> " +  mapData.replace("Age", "28", "45"));
        
        for (String strVal:mapData.keySet()){
            System.out.println("mapData.get(" + strVal + ") -> " + mapData.get(strVal));
        }
        
        System.out.println("mapData.containsKey(\"Age\") -> " +  mapData.containsKey("Age"));
        System.out.println("mapData.containsKey(\"Age123\") -> " +  mapData.containsKey("Age123"));
        
        System.out.println("mapData.get(\"Age123\") -> " +  mapData.get("Age123"));
        
        System.out.println("mapData.containsValue(\"Kos007\") -> " +  mapData.containsValue("Kos007"));
    }
    
}
