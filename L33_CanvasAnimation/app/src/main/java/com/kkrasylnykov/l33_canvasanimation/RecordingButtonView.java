package com.kkrasylnykov.l33_canvasanimation;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

public class RecordingButtonView extends ImageView implements View.OnClickListener {

	private DrawThread m_drowThread;
	private boolean m_bIsRun;
	private boolean m_bIsNoDestroy;

	private int m_nWidth;

	private int m_nCenter;
	private int m_nRadius;

	private int m_nHalfSquareSide;
	private ShapeDrawable m_DrawableBG = null;
	
	private Paint m_witePaint;
	private Paint m_redPaintSquare;
	private Paint m_redPaintArc;

	private Path m_PathCircl;
	private Path m_PathSquare;
	private Path m_PathArc;

	private RectF m_RectSquare;
	private RectF m_RectArc;

	private int m_nDeltaTime;
	private int m_nFPS;
	private long m_nOldTime;
	private float m_dDegrees;
	private float m_dIntervalDegrees;
	private int m_nCurrentCadr;
	private float m_dRadiusRounding;
	private float m_dIntervalRadiusRounding;
	private float m_dArcLength;
	private float m_dIntervalArcLength;
	private float m_dStartArc;

	private Matrix m_MatrixSquare;
	
	private final Object[] m_NativeWindowLock = new Object[0];

	public RecordingButtonView(Context context) {
		super(context);
		m_bIsRun = false;
		m_bIsNoDestroy = true;
		m_nWidth = -1;
		m_DrawableBG = new ShapeDrawable(new RectShape());
		this.setOnClickListener(this);
	}

	public RecordingButtonView(Context context, AttributeSet attrs) {
		super(context, attrs);
		m_bIsRun = false;
		m_bIsNoDestroy = true;
		m_nWidth = -1;
		m_DrawableBG = new ShapeDrawable(new RectShape());
		this.setOnClickListener(this);
	}



	public void startAnimate() {
		m_bIsRun = true;
	}

	public void stopAnimate() {
		m_bIsRun = false;
		m_bIsNoDestroy = false;
		
		m_nWidth = -1;
		
	}

	@Override
	protected void onDraw(Canvas canvas) {
		if (m_nWidth == -1) {
			m_nWidth = RecordingButtonView.this.getWidth();
			//m_nWidth = canvas.getWidth();
			
			m_nCenter = (int) (m_nWidth / 2);
			m_nRadius = m_nCenter - 2;

			m_nHalfSquareSide = (int) ((Math.sqrt(2) * m_nRadius - m_nRadius * 0.4) / 2);

			m_bIsRun = false;
			m_bIsNoDestroy = true;
			
			m_drowThread = new DrawThread();
			m_drowThread.start();
		}
		synchronized (m_NativeWindowLock) {
			canvas.drawPath(m_PathCircl, m_witePaint);
			canvas.drawPath(m_PathSquare, m_redPaintSquare);
			canvas.drawPath(m_PathArc, m_redPaintArc);
		}
		m_DrawableBG.draw(canvas);
		if (m_bIsNoDestroy) {
			this.invalidate();
		}
	}

	@Override
	public void onClick(View view) {
		startAnimate();
	}

	class DrawThread extends Thread {
		public DrawThread() {
			this.setName("DrawThread");
			setDefaultState();
		}

		public void setDefaultState() {
			m_nFPS = 18;
			m_nDeltaTime = (int) (1000 / m_nFPS);

			m_witePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
			m_redPaintSquare = new Paint(Paint.ANTI_ALIAS_FLAG);
			m_redPaintArc = new Paint(Paint.ANTI_ALIAS_FLAG);

			m_MatrixSquare = new Matrix();

			int StrokeWidthArc = (int) m_nWidth / 34;

			m_witePaint.setColor(Color.WHITE);
			m_witePaint.setStyle(Paint.Style.FILL_AND_STROKE);

			m_redPaintSquare.setColor(Color.RED);
			m_redPaintSquare.setStyle(Paint.Style.FILL_AND_STROKE);

			m_redPaintArc.setColor(Color.RED);
			m_redPaintArc.setStrokeWidth(StrokeWidthArc);
			m_redPaintArc.setStyle(Paint.Style.STROKE);

			m_PathCircl = new Path();
			m_PathSquare = new Path();
			m_PathArc = new Path();

			m_RectSquare = new RectF();
			m_RectArc = new RectF();

			m_RectSquare.set(m_nCenter - m_nHalfSquareSide, m_nCenter
					- m_nHalfSquareSide, m_nCenter + m_nHalfSquareSide,
					m_nCenter + m_nHalfSquareSide);
			m_RectArc.set(m_nCenter - (m_nRadius + 1 - StrokeWidthArc / 2),
					m_nCenter - (m_nRadius + 1 - StrokeWidthArc / 2), m_nCenter
							+ (m_nRadius + 1 - StrokeWidthArc / 2), m_nCenter
							+ (m_nRadius + 1 - StrokeWidthArc / 2));

			m_dRadiusRounding = m_nHalfSquareSide;
			m_dIntervalRadiusRounding = m_dRadiusRounding / m_nFPS;

			m_PathSquare.reset();
			m_PathSquare.addRoundRect(m_RectSquare, m_dRadiusRounding,
					m_dRadiusRounding, Path.Direction.CW);

			m_PathCircl.addCircle(m_nCenter, m_nCenter, m_nRadius,
					Path.Direction.CW);

			m_nOldTime = -1;
			m_dDegrees = 0;
			m_dIntervalDegrees = (float) ((float) 90 / (float) (m_nFPS - 2));
			m_nCurrentCadr = 0;
			m_dArcLength = 0;
			m_dIntervalArcLength = (float) ((float) 360 / ((float) (m_nFPS * 15)));
			m_dStartArc = 270;

			m_MatrixSquare.preRotate(m_dDegrees, m_nCenter, m_nCenter);
		}

		@Override
		public void run() {
			while (m_bIsNoDestroy) {
				long currentTime = System.currentTimeMillis();
				if (m_nOldTime >= currentTime) {
					continue;
				}
				m_nOldTime = currentTime + m_nDeltaTime;
				synchronized (m_NativeWindowLock) {
					if (m_bIsRun) {
						if (m_nCurrentCadr < (m_nFPS - 2)) {
							m_dDegrees += m_dIntervalDegrees;
							m_dRadiusRounding -= m_dIntervalRadiusRounding;

							m_MatrixSquare.reset();
							m_PathSquare.reset();

							m_MatrixSquare.preRotate(m_dDegrees, m_nCenter,
									m_nCenter);

							m_PathSquare.addRoundRect(m_RectSquare,
									m_dRadiusRounding, m_dRadiusRounding,
									Path.Direction.CW);
							m_PathSquare.transform(m_MatrixSquare);
						}
						m_PathArc.reset();
						m_PathArc.addArc(m_RectArc, m_dStartArc, m_dArcLength);
						m_dArcLength += m_dIntervalArcLength;
						m_nCurrentCadr++;
					}
				}
			}
		}
	}
}
