package com.kkrasylnykov.l17_fileandpermissionexample.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.kkrasylnykov.l17_fileandpermissionexample.fragments.ImageFragment;

public class ImageViewerAdapter extends FragmentPagerAdapter {

    private String[] m_arrData = null;

    public ImageViewerAdapter(FragmentManager fm, String[] arrData) {
        super(fm);
        m_arrData = arrData;
    }

    @Override
    public Fragment getItem(int position) {
        ImageFragment fragment = new ImageFragment();
        fragment.setPath(m_arrData[position]);
        return fragment;
    }

    @Override
    public int getCount() {
        return m_arrData.length;
    }
}
