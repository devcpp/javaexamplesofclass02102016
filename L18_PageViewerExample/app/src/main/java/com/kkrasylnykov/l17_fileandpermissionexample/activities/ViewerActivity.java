package com.kkrasylnykov.l17_fileandpermissionexample.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.Toast;

import com.kkrasylnykov.l17_fileandpermissionexample.R;
import com.kkrasylnykov.l17_fileandpermissionexample.adapters.ImageViewerAdapter;

import java.io.File;

public class ViewerActivity extends AppCompatActivity {

    public static final String KEY_FILE_PATHS = "KEY_FILE_PATHS";
    public static final String KEY_POSITION = "KEY_POSITION";

    private String[] m_arrPath = null;
    private ViewPager m_ViewPager = null;
    private ImageViewerAdapter m_Adapter = null;
   // private ImageView m_ImageView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewer);

        int nPosition = 0;
        Intent intent = getIntent();
        if (intent!=null){
            Bundle bundle = intent.getExtras();
            if (bundle!=null){
                m_arrPath = bundle.getStringArray(KEY_FILE_PATHS);
                nPosition = bundle.getInt(KEY_POSITION,0);
            }
        }

        if (m_arrPath==null){
            Toast.makeText(this, "Path not found!", Toast.LENGTH_LONG).show();
            finish();
        }

        m_ViewPager = (ViewPager) findViewById(R.id.ViewPager);
        m_Adapter = new ImageViewerAdapter(getSupportFragmentManager(), m_arrPath);
        m_ViewPager.setAdapter(m_Adapter);
        m_ViewPager.setCurrentItem(nPosition);
    }
}
