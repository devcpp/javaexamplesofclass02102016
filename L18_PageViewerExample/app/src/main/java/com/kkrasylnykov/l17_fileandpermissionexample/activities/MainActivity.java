package com.kkrasylnykov.l17_fileandpermissionexample.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.kkrasylnykov.l17_fileandpermissionexample.R;
import com.kkrasylnykov.l17_fileandpermissionexample.adapters.FileListViewAdapter;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private static final int REQUEST_PERMISSION_ON_READ_FILE = 201;

    private ArrayList<String> m_arrData = new ArrayList<>();
    private FileListViewAdapter m_adapter = null;
    private ListView m_listView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        m_listView = (ListView)findViewById(R.id.fileListView);
        m_listView.setOnItemClickListener(this);
        m_adapter = new FileListViewAdapter(m_arrData);
        m_listView.setAdapter(m_adapter);
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_ON_READ_FILE);
        } else {
            onInitData();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode== REQUEST_PERMISSION_ON_READ_FILE){
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED){
                Toast.makeText(this, "Get read storage permission", Toast.LENGTH_LONG).show();
                finish();
            } else {
                onInitData();
            }
        }
    }

    private void onInitData(){
        m_arrData.clear();
        m_arrData.addAll(getJPGFileListOnFile(Environment.getExternalStorageDirectory()));
        m_adapter.notifyDataSetChanged();
    }

    private ArrayList<String> getJPGFileListOnFile(File file){
        ArrayList<String> arrResult = new ArrayList<>();
        if (file!=null){
            if (file.isDirectory()){
                File[] arrFiles = file.listFiles();
                if (arrFiles!=null){
                    for (File curFile:arrFiles){
                        arrResult.addAll(getJPGFileListOnFile(curFile));
                    }
                }
            } else if (file.isFile()){
                if (file.getName().contains(".jpg")){
                    arrResult.add(file.getAbsolutePath());
                }
            }
        }
        return arrResult;
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        String[] stringArray = m_arrData.toArray(new String[0]);
        Intent intent = new Intent(this, ViewerActivity.class);
        intent.putExtra(ViewerActivity.KEY_FILE_PATHS, stringArray);
        intent.putExtra(ViewerActivity.KEY_POSITION, i);
        startActivity(intent);
    }
}
