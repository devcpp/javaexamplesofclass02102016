package com.kkrasylnykov.l17_fileandpermissionexample.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.kkrasylnykov.l17_fileandpermissionexample.R;

import java.io.File;
import java.util.HashMap;

public class ImageFragment extends Fragment {

    private String m_strPath = "";
    private ImageView m_ImageView = null;
    Thread m_thread = null;
    private volatile boolean m_bIsEndRun = false;
    private Bitmap myBitmap = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_image,null);
        m_ImageView = (ImageView) rootView.findViewById(R.id.ImageView);

        File imgFile = new  File(m_strPath);

        if(imgFile.exists()){
            myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            m_ImageView.setImageBitmap(myBitmap);
        } else {
            Toast.makeText(getActivity(), "File not found!", Toast.LENGTH_LONG).show();
            getActivity().finish();
        }
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        m_bIsEndRun = false;
        m_thread = new Thread(new Runnable() {
            @Override
            public void run() {
                final HashMap<Integer, Integer> mapColors = new HashMap<>();
                for (int x=0; x<myBitmap.getWidth(); x++){
                    for (int y=0; y<myBitmap.getHeight(); y++){
                        if (m_thread.isInterrupted()){
                            Log.d("devcpp", "m_thread.isInterrupted() -> " + m_thread.isInterrupted());
                            return;
                        }
                        int nColor = myBitmap.getPixel(x,y);
                        if (mapColors.containsKey(nColor)){
                            int nCount = mapColors.get(nColor) + 1;
                            mapColors.put(nColor,nCount);
                        } else {
                            mapColors.put(nColor,1);
                        }
                        int nC = 100;
                        int nB = 1000;
                        for (int n=0;n<nB; n++){
                            nC /=nC + n;
                        }
                    }
                }
                m_bIsEndRun=true;
                Log.e("devcpp", "m_thread is end run -> " + m_thread.getName());
                /*getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        m_thread = null;
                        Toast.makeText(getActivity(), "Colors count -> " + mapColors.keySet().size(),Toast.LENGTH_LONG).show();
                    }
                });*/
            }
        });
        m_thread.start();
        Log.e("devcpp", "m_thread is run -> " + m_thread.getName());

        while (true){
            if (m_bIsEndRun){
                Toast.makeText(getActivity(), "Colors count -> ",Toast.LENGTH_LONG).show();
                break;
            }
        }
    }

    public void setPath(String strPath){
        m_strPath = strPath;
    }

    @Override
    public void onStop() {
        super.onStop();
        if (m_thread!=null && m_thread.isAlive()){
            Log.d("devcpp", "m_thread.interrupt()");
            m_thread.interrupt();
        }
    }
}
