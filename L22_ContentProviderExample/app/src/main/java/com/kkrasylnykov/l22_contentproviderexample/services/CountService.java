package com.kkrasylnykov.l22_contentproviderexample.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.kkrasylnykov.l22_contentproviderexample.tools.AppSettings;

public class CountService extends Service {

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        AppSettings settings = new AppSettings(this);
        while (true){
            for (int i=1; i<=6; i++){
                Log.d("devcppService", "count -> " + i + " -> " + settings.getCountClick(i));
            }
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (false){
                break;
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
