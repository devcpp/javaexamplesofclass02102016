package com.kkrasylnykov.l22_contentproviderexample.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.kkrasylnykov.l22_contentproviderexample.services.CountService;


public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent serviceIntent = new Intent(context, CountService.class);
        context.startService(serviceIntent);
    }
}
