package com.kkrasylnykov.l22_contentproviderexample.contentProviders;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.util.Log;

public class AppSettingsContentProvider extends ContentProvider {
    public static final String KEY_DATA = "KEY_DATA";
    public static final String KEY_POSITION = "KEY_POSITION";
    private static final String KEY_COUNT_CLICK = "KEY_COUNT_CLICK";

    private static final String AUTHORITY = "com.kkrasylnykov.l22_contentproviderexample.contentProviders.AppSettingsContentProvider";

    public static final String COUNT_CLICK     = "count_click";

    public static final Uri COUNT_CLICK_URI    = Uri.parse("content://" + AUTHORITY + "/" + COUNT_CLICK);

    private static final int URI_COUNT_CLICK      = 1;

    private static final UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, COUNT_CLICK, URI_COUNT_CLICK);
    }

    private SharedPreferences m_SharedPreferences = null;

    @Override
    public boolean onCreate() {
        m_SharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        return false;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] strings, String s, String[] strings1, String s1) {
        Object returnData = null;
        Log.d("devcppCP","query -> " + uri);
        Log.d("devcppCP","query -> " + strings[0]);
        switch (uriMatcher.match(uri)) {
            case URI_COUNT_CLICK:
                returnData = new Integer(m_SharedPreferences.getInt(KEY_COUNT_CLICK+strings[0],0));
                Log.d("devcppCP","query -> " + (int)returnData);
                break;
        }
        String[] arrColumnNames = {KEY_DATA};
        Object[] arrValue = {returnData};
        MatrixCursor matrixCursor = new MatrixCursor(arrColumnNames);
        matrixCursor.addRow(arrValue);
        return matrixCursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        return null;
    }

    @Override
    public int delete(Uri uri, String s, String[] strings) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        Log.d("devcppCP","update -> " + uri);
        switch (uriMatcher.match(uri)) {
            case URI_COUNT_CLICK:
                int nCountExec = contentValues.getAsInteger(KEY_DATA);
                int nPosition = contentValues.getAsInteger(KEY_POSITION);
                Log.d("devcppCP","update -> nCountExec -> " + nCountExec);
                Log.d("devcppCP","update -> nPosition -> " + nPosition);
                SharedPreferences.Editor editor =  m_SharedPreferences.edit();
                editor.putInt(KEY_COUNT_CLICK+nPosition,nCountExec);
                editor.commit();
                break;
        }
        return 0;
    }
}
