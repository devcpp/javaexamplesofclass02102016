package com.kkrasylnykov.l22_contentproviderexample.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.kkrasylnykov.l22_contentproviderexample.R;
import com.kkrasylnykov.l22_contentproviderexample.tools.AppSettings;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        View button1 = findViewById(R.id.button1);
        button1.setOnClickListener(this);

        View button2 = findViewById(R.id.button2);
        button2.setOnClickListener(this);

        View button3 = findViewById(R.id.button3);
        button3.setOnClickListener(this);

        View button4 = findViewById(R.id.button4);
        button4.setOnClickListener(this);

        View button5 = findViewById(R.id.button5);
        button5.setOnClickListener(this);

        View button6 = findViewById(R.id.button6);
        button6.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        int nPosition =0;
        switch (view.getId()){
            case R.id.button1:
                nPosition = 1;
                break;
            case R.id.button2:
                nPosition = 2;
                break;
            case R.id.button3:
                nPosition = 3;
                break;
            case R.id.button4:
                nPosition = 4;
                break;
            case R.id.button5:
                nPosition = 5;
                break;
            case R.id.button6:
                nPosition = 6;
                break;
        }
        AppSettings appSettings = new AppSettings(this);
        Log.d("devcppApp", "count -> " + nPosition + " -> " + appSettings.addCountClick(nPosition));
    }
}
