package com.kkrasylnykov.l22_contentproviderexample.tools;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.preference.PreferenceManager;

import com.kkrasylnykov.l22_contentproviderexample.contentProviders.AppSettingsContentProvider;

public class AppSettings {

    private Context m_context;

    public  AppSettings (Context context){
        m_context = context;
    }

    public int addCountClick(int nPosition){
        int nResult = getCountClick(nPosition)+1;
        Uri uri = AppSettingsContentProvider.COUNT_CLICK_URI;
        ContentValues values = new ContentValues();
        values.put(AppSettingsContentProvider.KEY_DATA, nResult);
        values.put(AppSettingsContentProvider.KEY_POSITION, nPosition);
        m_context.getContentResolver().update(uri,values,null,null);
        return nResult;
    }

    public int getCountClick(int nPosition){
        int nResult = 0;
        Uri uri = AppSettingsContentProvider.COUNT_CLICK_URI;
        String[] arrData = new String[]{Integer.toString(nPosition)};
        Cursor cursor = m_context.getContentResolver().query(uri,arrData,null,null,null);
        if (cursor!=null && cursor.moveToFirst()){
            nResult = cursor.getInt(cursor.getColumnIndex(AppSettingsContentProvider.KEY_DATA));
        }
        return nResult;
    }


}
