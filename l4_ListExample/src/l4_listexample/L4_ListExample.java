package l4_listexample;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

public class L4_ListExample {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        LinkedList<Float> arrData = new LinkedList<>();
        
        Float fVal = new Float(0);
        do{
            fVal = scanner.nextFloat();
            arrData.add(fVal);
        }while (fVal>0);
        //arrData.remove(fVal);
        arrData.remove(arrData.size()-1);
        
        float fSum = 0;
        /*for (int i=0; i<arrData.size(); i++){
            fSum += arrData.get(i);
        }*/
        for (Float fVal1:arrData){
            fSum += fVal1;
        }
        fVal = new Float(3.5);
        int nPos = arrData.indexOf(fVal);
        
        System.out.println("nPos => " + nPos);
        
        System.out.println("fSum => " + fSum);
    }
    
}
