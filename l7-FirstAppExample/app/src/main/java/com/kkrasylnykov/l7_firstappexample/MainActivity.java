package com.kkrasylnykov.l7_firstappexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText m_EditText = null;
    private TextView m_textView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_linear);
        Log.d("devcppl7","onCreate -> ");

        m_textView = (TextView) findViewById(R.id.textViewMainActivity);
        //m_textView.setText("Test text");
        m_textView.setText(R.string.test_text_view);

        m_EditText = (EditText) findViewById(R.id.editTextMainActivity);

        Button button = (Button) findViewById(R.id.buttonMainActivity);
        button.setOnClickListener(this);

        Button buttonClean = (Button) findViewById(R.id.buttonCleanMainActivity);
        buttonClean.setOnClickListener(this);

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("devcppl7","onStart -> ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("devcppl7","onResume -> ");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("devcppl7","onPause -> ");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("devcppl7","onStop -> ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("devcppl7","onDestroy -> ");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonMainActivity:
                textToTextView();
                break;
            case R.id.buttonCleanMainActivity:
                cleanEditText();
                break;
        }
    }

    private void textToTextView(){
        String strText = m_EditText.getText().toString();
        Log.d("devcppl7","strText -> " + strText);
        if (!strText.isEmpty()){
            m_textView.setText(strText);
            m_EditText.setText("");
        } else {
            Log.e("devcppl7_error","strText -> isEmpty!!!");
        }

    }

    private void cleanEditText(){
        m_EditText.setText("");
    }
}
