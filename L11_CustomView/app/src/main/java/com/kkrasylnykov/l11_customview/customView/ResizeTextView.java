package com.kkrasylnykov.l11_customview.customView;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.widget.TextView;

import com.kkrasylnykov.l11_customview.R;

public class ResizeTextView extends TextView {
    private float m_fMinTextSize = 0;

    public ResizeTextView(Context context) {
        super(context);
    }

    public ResizeTextView(Context context, AttributeSet attrs) {

        super(context, attrs);
        setAttr(context, attrs);
    }

    public ResizeTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setAttr(context, attrs);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ResizeTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setAttr(context, attrs);
    }

    private void setAttr(Context context, AttributeSet attrs){
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/AdmirationPains.ttf");
        this.setTypeface(font);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.ResizeTextView,
                0, 0);

        try {
            m_fMinTextSize = a.getDimensionPixelSize(R.styleable.ResizeTextView_minTextSize, 0);
        } finally {
            a.recycle();
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int nModeW = MeasureSpec.getMode(widthMeasureSpec);
        int nWidth = MeasureSpec.getSize(widthMeasureSpec);
        int nModeH = MeasureSpec.getMode(heightMeasureSpec);
        int nHeight = MeasureSpec.getSize(heightMeasureSpec);
        super.onMeasure(MeasureSpec.UNSPECIFIED, heightMeasureSpec);
        int nRealWidth = this.getMeasuredWidth();
        Log.d("devcppSize","nWidth -> " + nWidth);
        Log.d("devcppSize","nRealWidth -> " + nRealWidth);
        float fDelta = ((float)nWidth)/((float)nRealWidth);
        if (fDelta<1) {
            float fTextSize = this.getTextSize() * fDelta;
            Log.d("devcppSize", "fTextSize -> " + fTextSize);
            this.setTextSize(TypedValue.COMPLEX_UNIT_PX, fTextSize);
            super.onMeasure(MeasureSpec.UNSPECIFIED, heightMeasureSpec);
            nRealWidth = this.getMeasuredWidth();
            while (nRealWidth > nWidth) {
                fTextSize = this.getTextSize() - 5;
                this.setTextSize(TypedValue.COMPLEX_UNIT_PX, fTextSize);
                super.onMeasure(MeasureSpec.UNSPECIFIED, heightMeasureSpec);
                nRealWidth = this.getMeasuredWidth();
            }
            if (m_fMinTextSize>fTextSize){
                fTextSize = m_fMinTextSize;
                this.setTextSize(TypedValue.COMPLEX_UNIT_PX, fTextSize);
                this.setLines(1);
            }
        }

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
