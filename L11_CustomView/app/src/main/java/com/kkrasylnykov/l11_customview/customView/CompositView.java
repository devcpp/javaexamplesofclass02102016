package com.kkrasylnykov.l11_customview.customView;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kkrasylnykov.l11_customview.R;

public class CompositView extends RelativeLayout {
    private ImageView m_image = null;
    private TextView m_text = null;

    public CompositView(Context context) {
        super(context);
        initLayaut(context);
    }

    public CompositView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initLayaut(context);
    }

    public CompositView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initLayaut(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public CompositView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initLayaut(context);
    }

    private void initLayaut(Context context){
        LayoutInflater.from(context).inflate(R.layout.custom_view_composit,this,true);
        m_image = (ImageView) findViewById(R.id.image);
        m_text = (TextView) findViewById(R.id.text);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int nWidth = MeasureSpec.getSize(widthMeasureSpec);
        int nImageWidth = m_image.getMeasuredWidth() + 25;
        ((RelativeLayout.LayoutParams)m_text.getLayoutParams()).setMargins(nImageWidth, 0,0,0);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        while (m_image.getMeasuredHeight() < m_text.getMeasuredHeight()){
            m_text.setTextSize(TypedValue.COMPLEX_UNIT_PX, m_text.getTextSize() - 1);
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }
}
