package l31_mediatorexample;

public abstract class Mediator {
    public abstract void send(String message, Colleague colleague);
}
