package l31_mediatorexample;

import java.util.ArrayList;

public class ConcreteMediator extends Mediator{
    
    ArrayList<Colleague> m_arrColleagues;

    public ConcreteMediator() {
        m_arrColleagues = new ArrayList<>();
    }    
 
    public void addColleague(Colleague colleague){
        m_arrColleagues.add(colleague);
    }
    
    public void removeColleague(Colleague colleague){
        m_arrColleagues.remove(colleague);
    }

    @Override
    public void send(String message, Colleague colleague) {
        for(Colleague item:m_arrColleagues){
            if (!item.equals(colleague)){
                item.notify(message);
            }
        }
    }
    
}
