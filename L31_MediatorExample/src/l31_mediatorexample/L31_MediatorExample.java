package l31_mediatorexample;

public class L31_MediatorExample {

    public static void main(String[] args) {
        
        ConcreteMediator mediator = new ConcreteMediator();
        
        Colleague c1 = new Colleague(1, mediator);
        Colleague c2 = new Colleague(2, mediator);
        Colleague c3 = new Colleague(3, mediator);
        Colleague c4 = new Colleague(4, mediator);
        
        mediator.addColleague(c1);
        mediator.addColleague(c2);
        mediator.addColleague(c3);
        mediator.addColleague(c4);
        
        c1.send("Hello!!!");
        c2.send("hi!");
        c3.send("hi!!!!");
        c4.send("hi!!");
    }
    
}
