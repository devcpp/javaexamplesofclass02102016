package l31_mediatorexample;

public class Colleague {
    int m_nId;
    Mediator m_mediator;
    
    public Colleague(int nId, Mediator mediator){
        m_nId = nId;
        m_mediator = mediator;
    }
    
        
    public void send(String message) {
        m_mediator.send(message, this);
    }
    
    public void notify(String message) {
        System.out.println("Colleague" + m_nId +" gets message: " + message);
    }
    
}
